**Painel Ponto.UA**  
**Manual de Instalação**

A aplicação para o painel Ponto.UA assenta sobre tecnologias web, utilizando
diversas tecnologias como PHP, Node.JS e HTML para que todo o serviço funcione
na perfeição. Resulta numa aplicação interativa multitouch para um painel
multitouch Displax, com complemento de um site para smartphone que interage com
a aplicação do painel.  
Para efeitos práticos, assumiremos que o serviço de ficheiros e PHP está no
domínio **pontoua.pneves.pt** e o serviço de Node.JS (com socket.io) está no
domínio **socket.pontoua.pneves.pt**.  
O primeiro é responsável por servir os ficheiros para o painel interativo,
servir os ficheiros do backoffice e guardar a base-de-dados.  
O segundo é responsável por servir o Node.JS e os ficheiros para o smartphone.  
É necessário preencher os dados de configuração e serviços nos ficheiros
**config.js** e **config.php**.  
As portas definidas podem ser alteradas, mas é necessário cautela a eventuais
conflitos, principalmente para arrancar o serviço do Node.JS. Não é necessário
utilizar as mesmas portas que as definidas neste esquema, desde que devidamente
configuradas nos ficheiros de configuração. O serviço HTTP tem que ser servido
sempre na porta 80.  
Esta aplicação recolhe dados do Facebook da Universidade de Aveiro para as poder
apresentar no painel, pelo que é necessário aceder ao backoffice (em
**pontoua.pneves.pt/backoffice.php**) para fazer login no Facebook.  
É necessário um email válido (e os dados de SMTP) para enviar um email de
notificação acerca da expiração da sessão do Facebook. Esta funcionalidade não é
necessária e é experimental, pelo que é recomendável atenção à atualização dos
álbuns de fotografias no painel interativo.  
É necessário que o servidor onde corre o backoffice tenha permissões de escrita
na pasta “assets”.  
A base-de-dados pode ser criada com o ficheiro database.sql e o seu modelo
encontra-se na pasta database\_mode.mwb

**Esquema da versão QA:**

![http://pneves.pt/assets/pontoua.jpg](http://pneves.pt/assets/pontoua.jpg)

**Backoffice:**  
  
Para aceder ao backoffice é necessário realizar login, que, inicialmente utiliza
a password “secret”. Além de uma password de acesso, é necessário realizar login
com o Facebook para poderem ser carregados os álbuns do painel interativo.  
No backoffice podem-se fazer as seguintes configurações:  
- Forçar a atualização dos álbuns;  
- Excluir álbuns diretamente. Para esta funcionalidade são carregados os últimos
20 álbuns, sendo que são carregados no painel os últimos 13 álbuns válidos.
Clica-se no botão ao lado do álbum para excluir/mostrar;  
- Excluir álbuns por nome. Esta funcionalidade é utilizada para excluir álbuns
por nome e não por álbum. São carregadas inicialmente quatro regras de exclusão:
“Untitled Album”, “Timeline Photos”, “Cover Photos” e “Vídeos”. Esta
funcionalidade auxilia a que certos álbuns sejam excluídos de forma automática
sem ser necessária intervenção de um administrador;  
- Adicionar uma nova regra de exclusão por nome (referente ao ponto anterior).
Esta funcionalidade é guardada com o botão GUARDAR na barra inferior;  
- Definir a cantina pré-definida;  
- Alterar, ocultar ou mostrar departamentos inseridos. Esta funcionalidade é
guardada com o botão GUARDAR na barra inferior;  
- Adicionar um novo departamento. Esta funcionalidade é guardada com o botão
GUARDAR na barra inferior;  
- Fazer upload do background do mapa. Esta funcionalidade é guardada com o botão
GUARDAR na barra inferior;  
- Alterar a password de acesso ao backoffice. Esta funcionalidade é guardada com
o botão GUARDAR na barra inferior;  
- Alterar o email de notificação de expiração do acesso ao Facebook. Esta
funcionalidade é guardada com o botão GUARDAR na barra inferior.

**Tecnologias necessárias:**  
- PHP;  
- Apache ou nginx;  
- MySQL;  
- Node.JS;  
- SMTP (email);  
- Git.

**Instalação numa máquina privada:**  
Este tutorial de instalação servirá para instalar o serviço numa máquina
privada, acessível através de uma Intranet ou LAN, onde o servidor é também o
computador ligado ao ecrã multitoque.  
Essa máquina terá que ter Sistema Operativo Windows e login automático numa
conta quando o computador é ligado.  
Nota 1: As definições dos ficheiros config.js e config.php são as definições
requeridas por este tutorial.  
Nota 2: É necessário que o computador seja acessível a partir de telemóveis
ligados à rede wifi a partir das portas definidas no ficheiro config.js.

1.  Configurar o Windows para fazer login automático na conta que irá executar
    os serviços;

2.  Instalar o Chrome, GIT e Node.JS (atenção que o GIT e Node.JS devem ser,
    durante a instalação ou manualmente, adicionado às Variáveis de Ambiente -
    PATH - do Windows e o Node.JS tem que ser a versão 0.10.9
    https://nodejs.org/dist/v0.10.9/node-v0.10.9-x86.msi);

3.  Instalar XAMPP;

4.  Navegar com a linha de comandos do windows para a pasta htdocs dentro da
    pasta de instalação do XAMPP;

5.  Dentro da pasta htdocs, correr o comando “git clone
    <https://bitbucket.org/pauloallex24/ponto-ua.git>” (poderá ser necessário
    fazer login com a conta do bitbucket);

6.  Abrir o notepad com permissões de administrador e após a sua abertura, abrir
    o ficheiro “C:\\Windows\\System32\\drivers\\etc\\hosts” e adicionar no final
    do ficheiro o conteúdo “127.0.0.1 montra.pneves.pt” (este domínio é
    fictício, mas é necessário para que o Facebook autorize a aquisição de dados
    da sua API);

7.  Abrir (também com permissões de administrador) o ficheiro
    “C:\\xampp\\apache\\conf\\httpd.conf” e adicionar no final do ficheiro o
    conteúdo:  
    **Alias /ponto-ua “C:/xampp/htdocs/ponto-ua/”**  
    **\<Directory “C:/xampp/htdocs/ponto-ua/”\>**  
    **AllowOverride All**  
    **Order allow,deny**  
    **Allow from all**  
    **\</Directory\>**

8.  Abrir (também com permissões de administrador) o ficheiro
    “C:\\xampp\\apache\\conf\\extra\\httpd-vhosts.conf” e adicionar no final do
    ficheiro o conteúdo:  
    **NameVirtualHost \*:80**  
    **\<VirtualHost \*:80\>**  
    **ServerName montra.pneves.pt**  
    **ServerAlias montra.pneves.pt**  
    **ServerAdmin** <paulomneves@ua.pt>  
    **DocumentRoot “C:/xampp/htdocs/ponto-ua/”**  
    **\</VirtualHost\>**

9.  Abrir o painel do XAMPP e correr os serviços APACHE e MySQL;

10. Após estes dois terem iniciado, aceder a “montra.pneves.pt/phpmyadmin”.

11. No phpmyadmin carregar em “Importar”, navegar até à pasta htdocs/ponto-ua e
    importar o ficheiro “database.sql”;

12. É agora necessário uma aplicação do Facebook Developers. Nesta aplicação são
    fundamentais as seguintes definições:  
    - Settings: Adicionar plataforma “website” com o domínio “montra.pneves.pt”
    e o campo “App Domains” também com “montra.pneves.pt”;  
    - Adicionar o Produto (na barra da esquerda) “Facebook Login”: com “Client
    OAuth Login” “Web OAuth Login” e “Embedded Browser OAuth Login” ativos. No
    Campo “Valid OAuth redirects URIs” deve estar o conteúdo
    “http://montra.pneves.pt/backoffice.php”;  
    - Em “App Review” (da barra da esquerda) a aplicação deve estar pública;

13. Ir ao Dashboard da aplicação do Facebook developers e copiar a AppID e a
    AppSecret para o ficheiro config.php. Neste ficheiro devem ser também agora
    preenchidos os dados da base-de-dados, domínio (montra.pneves.pt) e poderão
    já ser preenchidos ou não os dados de SMTP.

14. Para o SMTP é necessário um email, uma password (do email) e os dados do
    servidor de SMTP e este email é enviado para informar acerca do token do
    Facebook estar a expirar e ser necessário um login no backoffice para
    renovar esse token;

15. Alterar também o ficheiro config.js, “domain” para “http://montra.pneves.pt”
    e “socket\_domain” para o IP da máquina na LAN (ex: 192.168.1.5). Alterar
    também a porta disponível na rede para os sockets. Está predefinida como
    3010 e provavelmente não funcionará na porta 80;

16. No painel do XAMPP entrar em “config” (da barra direita do painel) e colocar
    em “autostart of modules” o modulo do Apache e do MySQL.

17. Criar atalhos dos ficheiros “run\_pontoua\_s1.bat” e “run\_pontoua\_s2.bat”
    e colocar esses atalhos na pasta “%AppData%\\Microsoft\\Windows\\Start
    Menu\\Programs\\Startup\\”

**Dados de contacto:**  
A parte lógica da aplicação foi desenvolvida por:  
Paulo Neves – <paulomneves@ua.pt>  
Em colaboração com:  
Pedro Almeida – <almeida@ua.pt>  
Pedro Beça – <pedrobeca@ua.pt>