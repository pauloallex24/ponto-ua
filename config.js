var domain = "http://montra.pneves.pt";
var socket_domain = "http://192.168.1.5";
var socket_service_port = 3010;
var socket_server_port = 3010;  //Esta porta deve ser diferente de socket_service_port caso seja utilizado reverse proxy (sendo esta a porta do reverse proxy). Caso contrário deve ser igual à socket_service_port
var debug = true;

try {
    exports.config = {domain: domain, socket_domain: socket_domain, port: socket_service_port, debug: debug};
} catch(e) {
    console.log('exports error');
}
