/*$('.excluir_albuns span.input-group-addon').click(function() {

 $($(this).children('input[type="checkbox"]')).prop('checked',
 !($(this).children('input[type="checkbox"]')).prop('checked'));

 if(!$($(this).children('input[type="checkbox"]')).prop('checked')) {
 $(this).removeClass('btn-warning');
 $(this).removeClass('glyphicon-minus');
 $(this).addClass('btn-success');
 $(this).addClass('glyphicon-ok');
 $($(this).children('span.titulo')).html('A Mostrar');
 }
 else {
 $(this).addClass('btn-warning');
 $(this).addClass('glyphicon-minus');
 $(this).removeClass('btn-success');
 $(this).removeClass('glyphicon-ok');
 $($(this).children('span.titulo')).html('Ocultado');
 }
 });

 $('#cantina_predefinida .btn').click(function(){
 $('#cantina_predefinida .btn').removeClass('btn-primary').addClass('btn-default');
 $($(this).children('input[type="checkbox"]')).prop('checked',true);
 $(this).addClass('btn-primary');

 });

 $('#departamentos .panel-heading div.btn').click(function(){
 $($($(this).parent()).children('div.btn'))
 .removeClass('btn-info')
 .addClass('btn-default');

 $($(this).children('input')).prop('checked',true);
 $(this).removeClass('btn-default')
 .addClass('btn-info');

 });
 */
$('a.ocultar').click(function(){
    return confirm("A alteração será realizada de imediato!");
});
$('a.delete').click(function(){
    return confirm("Pretende apagar?");
});
$('#reload_pannel').click(function(){
    if(confirm("O Painel vai atualizar e interromper todas as interações. Avançar?")) {
        socket.emit('reload');
    } else {
        return false;
    }
});

var elem;
$("input[type='file'].dep").change(function(){
    if($(this).attr('code')>0) {
        elem = '_' + $(this).attr('code');
        console.log('a: ' + elem);
    }
    var files = !!this.files ? this.files : [];
    if ( !files.length || !window.FileReader ) return;
    if ( /^image/.test( files[0].type ) ) {
        var reader = new FileReader();
        reader.readAsDataURL( files[0] );
        reader.onloadend = function(){
            console.log(elem);
            $("#show" + ((elem)?elem:'')).css("background-image", "url(" + this.result + ")");

        }

    }

});