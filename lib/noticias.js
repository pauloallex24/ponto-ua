function noticias(father) {
    /*this.x = x;
    this.y = y;
    this.title = title;
    this.type = type;
    this.parent = parent;
    this.window = {};
    this.x = 0;*/
    /*do{
     this.id = 'p' + parseInt(Math.random()*99999);
     } while($('#' + this.id).length != 0);
    this.id = 'p' + count;*/
    this.father = father;

    this.init = function() {
        $('body').append('<div class="noticias"></div>');
        $('.noticias')[0].element = this;
        for(i=0;i<10;i++) {
            $('.noticias').append('<div id="n' + i + '" class="noticia touch"' +
            'style="left: ' + this.formula(this,i) + 'px; z-index: ' + (200-10*i) + ';"' +
            '><div class="inner" style="animation-delay:-' + (1-i*0.2) + 's;" onclick="$(\'.noticias\')[0].element.move('+i+',this);app.now = new Date().getTime();"></div></div>');
            $('#n' + i + ' .inner').append('<div class="image"><img src="assets/img/edificios/reitoria.jpg"></div>');
            $('#n' + i + ' .inner').append('<div class="title">Unknown Error</div>');
            $('#n' + i + ' .inner').append('<div class="info">Unknown Error</div>');
        }
        this.refresh();
    };


    this.hide = function() {
        for(i=0;i<10;i++) {
            $('#n' + i).css({display:'none'});
        }
    };

    this.open = function() {
        menuLocation = 'noticias';
        dragPos = 1999;
        this.father.hide();
        app.saver.green();
        for(i=0;i<10;i++) {
            $('#n' + i).css({display:'none'});
        }
        setTimeout(function(t){
            for(i=0;i<10;i++) {
                $('#n' + i).css({display:'block',left: t.formula(t,i)});
            }
        },10,this);
        socket.emit('noticia',{
            image:$($($('.noticias .inner')[0]).children('.image')).children('img').attr('src'),
            title:$($('.noticias .inner')[0]).children('.title').text(),
            link:$($('.noticias .inner')[0]).attr('link')
        });
        $('.noticias').append('<div class="hand"></div>');
        setTimeout(function(){$('.hand').remove()},2500);
        app.now = new Date().getTime();
    };

    this.close = function() {
    };

    this.updatePos = function() {
        for(i=0;i<10;i++) {
            $('#n' + i).css('left',this.formula(this,i));
        }
    };

    this.formula = function(e,i) {
        return ( Math.sin(((e.father.touch.x-1920)/1920)+(i/10)) *  ((1920*2)/((i+1)/(3)))  +  300);
        console.log((Math.sin((e.father.touch.x))));
        return (300 * i) * Math.tan(1-(i/10));

    };

    this.move = function (index,obj) {
        if(!obj) {
            obj = $('.noticia')[index];
            //console.log($($($(obj)[0]).children('.image')).children('img'));
        }
        //console.log('move');
        socket.emit('noticia',{
            image:$($($($(obj)[0]).children('.inner')).children('.image')).children('img').attr('src'),
            title:$($($(obj)[0]).children('.inner')).children('.title').text(),
            link:$($(obj)[0]).attr('link')
        });
        var arr = [
            [300,875,1064,1152,1198,1221,1230,1231,1231,1231],//0
            [-409,360,875,1064,1152,1198,1221,1230,1231,1231],//1
            [-1118,-409,360,875,1064,1152,1198,1221,1230,1231],//2
            [-1118,-1118,-409,360,875,1064,1152,1198,1221,1230],//3
            [-1118,-1118,-1118,-409,360,875,1064,1152,1198,1221],//4
            [-1118,-1118,-1118,-1118,-409,360,875,1064,1152,1198],//5
            [-1118,-1118,-1118,-1118,-1118,-409,360,875,1064,1152],//6
            [-1118,-1118,-1118,-1118,-1118,-1118,-409,360,875,1120],//7
            [-1118,-1118,-1118,-1118,-1118,-1118,-1118,-409,360,1120],//8
            [-1118,-1118,-1118,-1118,-1118,-1118,-1118,-409,360,1120]//9
        ];
        for(var i=0;i<10;i++) {
            $('#n' + i).css({left:arr[index][i]});
        }
    };

    this.refresh = function() {
        $.ajax({
            url: "http://services.sapo.pt/UA/Online/contents_xml?jsonText=true&n=10&dt=1"
        })
            .done(function (data) {

                noticias = [];

                data = jQuery.parseJSON(data);

                var title = "";
                var content = "";
                var img = "";
                var link = "";
                var qr = "";

                for (var i = 0; i < data.rss.channel.item.length; i++) {

                    data.rss.channel.item[i].description = utf8_decode(data.rss.channel.item[i].description);
                    title = utf8_decode(data.rss.channel.item[i].title);
                    link = data.rss.channel.item[i].link;
                    //qr = 'https://chart.googleapis.com/chart?cht=qr&chs=250x250&chl=' + link;

                    content = data.rss.channel.item[i].description;
                    if (content.indexOf('src="') > 0) {
                        img = (content.substr(content.indexOf('src="') + 5, content.indexOf('" alt=') - content.indexOf('src="') - 5)).replace('_thumb', '');
                        content = content.substr(content.indexOf('/>') + 2);
                    }
                    else {
                        img = '';
                        content = content.substr(5);
                    }

                    if (img == "")
                        img = 'img/reitoria.png';

                    if (content.length > 320) {
                        var trimmedString = content.substr(0, 320);
                        content = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
                        content += '';
                    }

                    $('#n' + i + ' .inner img').attr('src',img);
                    $('#n' + i + ' .inner .title').text(title);
                    $('#n' + i + ' .inner .info').text(content + ' (...)');
                    var aux = 26, aux2 = 45;
                    do{
                        $('#n' + i + ' .inner').attr('link',link);
                        $('#n' + i + ' .inner .title').css({'font-size':aux2});
                        $('#n' + i + ' .inner .info').css({'font-size':aux--,'line-height':aux2-- + 'px'});
                        aux2-=2;
                    } while(($('#n' + i + ' .inner img').height() + $('#n' + i + ' .inner .title').height() + $('#n' + i + ' .inner .info').height() + 50)>1030);
                    //noticias.push({title: title, content: content, img: img, link: link, qr: qr});

                }


            }

        ).error(function(){
                console.log("error getting data");
            });
    };

    this.init();

}