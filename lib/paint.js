function painter(father) {

    this.father = father;
    this.opened = false;

    this.init = function() {
        console.log('init');
        $('body').append('<div class="paint" id="canvasDiv">' +
        '<div class="color" style="background-color: #ADFF35;left:700px;" onclick="curColor=\'#ADFF35\'"></div>' +
        '<div class="color" style="background-color: #ffc511;left:900px;" onclick="curColor=\'#ffc511\'"></div>' +
        '<div class="color" style="background-color: #25c0d1;left:1100px;" onclick="curColor=\'#25c0d1\'"></div>' +
        '<div class="color" style="background-color: #c02f2a;left:1300px;" onclick="curColor=\'#c02f2a\'"></div></div>');
         canvasDiv = document.getElementById('canvasDiv');
         colorPurple = "#25c0d1";
         colorGreen = "#ADFF35";
         colorYellow = "#ffc511";
         colorBrown = "#c02f2a";
        curColor = colorGreen;
        clickColor = new Array();
        canvas = document.createElement('canvas');
        canvas.setAttribute('width', 1920);
        canvas.setAttribute('height', 1080);
        canvas.setAttribute('id', 'canvas');
        canvasDiv.appendChild(canvas);
        if(typeof G_vmlCanvasManager != 'undefined') {
            canvas = G_vmlCanvasManager.initElement(canvas);
        }
        context = canvas.getContext("2d");
        $('#canvas').mousedown(function(e){
            var mouseX = e.pageX - this.offsetLeft;
            var mouseY = e.pageY - this.offsetTop;

            paint = true;
            addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
            redraw();

        });
        $('#canvas').mousemove(function(e){
            if(paint){
                addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
                redraw();
            }
        });
        $('#canvas').mouseup(function(e){
            paint = false;
        });

        clickX = new Array();
        clickY = new Array();
        clickDrag = new Array();
        paint = false;

    };


    this.hide = function() {
        $('.paint').css('display','none');
    };


    this.open = function(id) {
        this.refresh(id);
        this.father.hide();
        $('.paint').css('display','block');
        app.now = new Date().getTime();
        clearDraw();
    };

    this.close = function() {

    };

    this.updatePos = function() {

    };

    this.formula = function(e,i) {

    };

    this.refresh = function(id) {

    };
    this.init();

}

function addClick(x, y, dragging)
{
    try {
        app.now = new Date().getTime();
        clickX.push(x);
        clickY.push(y);
        clickDrag.push(dragging);
        clickColor.push(curColor);
    }
    catch (e) {
        console.log('paint2');
    }
}
function redraw(){
    try {
        app.now = new Date().getTime();
        context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas


        context.lineJoin = "round";
        context.lineWidth = 15;

        for(var i=0; i < clickX.length; i++) {
            context.beginPath();
            if(clickDrag[i] && i){
                context.moveTo(clickX[i-1], clickY[i-1]);
            }else{
                context.moveTo(clickX[i]-1, clickY[i]);
            }
            context.lineTo(clickX[i], clickY[i]);
            context.closePath();
            context.strokeStyle = clickColor[i];
            context.stroke();
        }

    }
    catch (e) {
        console.log('paint');
    }
}

function clearDraw(){
    try {
        context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas

        context.strokeStyle = "#df4b26";
        context.lineJoin = "round";
        context.lineWidth = 5;

        clickX = new Array();
        clickY = new Array();
        clickDrag = new Array();
        curColor = colorGreen;
        clickColor = new Array();
        paint = false;

    }
    catch (e) {
        console.log('paint');
    }
}