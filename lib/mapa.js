function mapa(father) {
    /*this.x = x;
    this.y = y;
    this.title = title;
    this.type = type;
    this.parent = parent;
    this.window = {};
    this.x = 0;*/
    /*do{
     this.id = 'p' + parseInt(Math.random()*99999);
     } while($('#' + this.id).length != 0);
    this.id = 'p' + count;*/
    this.father = father;

    this.init = function() {
        $('body').append('<div class="mapa"></div>');
        $('.mapa').append('<div class="map_viewer" style="opacity:0;" onclick="$(this).css({opacity:0});setTimeout(function(){$(\'.map_viewer\').css({display:\'none\'});},500);"><div class="viewer"><div class="cover"></div><div class="conteudo"></div></div></div>');
        $('.mapa')[0].element = this;

        this.refresh();
    };


    this.hide = function() {
        return true;
    };

    this.open = function() {
        menuLocation = 'mapa';
        this.father.hide();
        dragPos = 1999;
        $('.mapa .departamento').remove();
        $('.mapa').css({display: 'block','background-image':'url(assets/buildings/mapa.jpg)'});
        for(var i=0;i<mapa_data.length;i++) {
            console.log(mapa_data);
            this.newDep(i);
        }
        setTimeout(function(){$('.mapa').css({opacity:1});},100);


        app.now = new Date().getTime();
    };

    this.close = function() {
        $('.map_viewer').css({opacity:0});setTimeout(function(){$('.map_viewer').css({display:'none'});},500);
        setTimeout(function(){$('.mapa').css({display: 'none',opacity:0});},500);
    };

    this.newDep = function(d) {
        console.log(d);
        $('.mapa').append('<div class="departamento" onclick="' +
            '$(\'.mapa\')[0].element.departamento(' + d + ')"' +
            'style="left:' + mapa_data[d].x + 'px;top:'
            + mapa_data[d].y + 'px;">'
            + mapa_data[d].acronym + '</div>');
    };

    this.departamento = function(d) {
        console.log("DEP");
        $('.map_viewer').css({display:'block'});
        $('.map_viewer .cover').css({'background-image':'url(assets/buildings/' + mapa_data[d].picture + ')'});
        $('.map_viewer .conteudo').html('<h1>' + mapa_data[d].name + '</h1><br>' + mapa_data[d].content);
        setTimeout(function(){$('.map_viewer').css({opacity:1})},50);

        toDataUrl(domain + '/assets/buildings/' + mapa_data[d].picture, function(base64Img,d) {
            console.log(d);
            socket.emit('noticia',{
                image: base64Img,
                title:mapa_data[d].name,
                link:mapa_data[d].url
            });
        },d);
/*
        socket.emit('noticia',{
            image: domain + '/assets/buildings/' + mapa_data[d].picture,
            title:mapa_data[d].name,
            link:mapa_data[d].url
        });*/
    };

    this.formula = function(e,i) {
        return true;
    };

    this.move = function (index,obj) {
        return true;
    };

    this.refresh = function() {
        $.ajax({
                url: "./backoffice/api.php?mapa=1"
            })
            .done(function (data) {
                //mapa = $.parseJSON(data);
                mapa_data = data['mapa'];
                console.log(mapa_data);
            });
    };

    this.init();

}
var mapa_data = {};

//http://stackoverflow.com/questions/6150289/how-to-convert-image-into-base64-string-using-javascript
function toDataUrl(url, callback, index) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
        var reader = new FileReader();
        reader.onloadend = function() {
            callback(reader.result, index);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
}