function ementa(father) {

    this.father = father;
    ementas = {};
    this.default = 0;

    this.init = function() {
        $('body').append('<div class="ementa touch"></div>');
        $('.ementa')[0].element = this;
        /*$('.ementa')
         .append('<div class="whitebar"></div>');*/

        var i = 0;
        $('.ementa')
            .append('<div class="cantina touch" id="c' + i + '" style="left:'+(300+i*400+i*60)+'px;z-index:'+(200-i*10)+
                '"onclick="event.stopPropagation();app.dots[1].child.move(0)"' +
                '><div class="image" style="animation-delay:-' + (1-i*0.2) + 's;z-index:'+(200-i*10)+
                '"></div><div class="info"style="z-index:'+(100-i*10)+' !important;"' +
                '></div></div>');
        i = 1;
        $('.ementa')
            .append('<div class="cantina touch" id="c' + i + '" style="left:'+(300+i*400+i*60)+'px;z-index:'+(200-i*10)+
                '"onclick="event.stopPropagation();app.dots[1].child.move(1)"' +
                '><div class="image" style="animation-delay:-' + (1-i*0.2) + 's;z-index:'+(200-i*10)+
                '"></div><div class="info" style="z-index:'+(100-i*10)+' !important;"' +
                '></div></div>');
        i = 2;
        $('.ementa')
            .append('<div class="cantina touch" id="c' + i + '" style="left:'+(300+i*400+i*60)+'px;z-index:'+(200-i*10)+
                '"onclick="event.stopPropagation();app.dots[1].child.move(2)"' +
                '><div class="image" style="animation-delay:-' + (1-i*0.2) + 's;z-index:'+(200-i*10)+
                '"></div><div class="info" style="z-index:'+(100-i*10)+' !important;"' +
                '></div></div>');
        /*switch (i) {
         case 0: $('#c' + i).css('background-image','url(assets/img/edificios/cantina_santiago.jpg)');break;
         case 1: $('#c' + i).css('background-image','url(assets/img/edificios/cantina_crasto.jpg)');break;
         case 2: $('#c' + i).css('background-image','url(assets/img/edificios/snack_bar.jpg)');break;
         }*/

        //for(i=0;i<$('.ementa').length - 1;i++) {
        $('.ementa .cantina .info')
            .append('<table><tr class="principal"><td rowspan="3" class="title rotate-90">Almoço</td><td class="type">principal</td><td class="hoje"></td><td class="amanha"></td><td class="alem"></td><td class="depois"></td></tr>' +
                '<tr class="extra"><td class="type">extra</td><td class="hoje"></td><td class="amanha"></td><td class="alem"></td><td class="depois"></td></tr>' +
                '<tr class="vegetariano"><td class="type">vegeriano</td><td class="hoje"></td><td class="amanha"></td><td class="alem"></td><td class="depois"></td></tr></table>')
            .append('<table class="week"><tr><td class="title">&nbsp;</td><td class="type">&nbsp;</td><td class="hoje">hoje</td><td class="amanha">amanhã</td><td class="alem">quarta</td><td class="depois">quinta</td></tr></table>')
            .append('<table><tr class="principal"><td rowspan="3" class="title rotate-90">Jantar</td><td class="type">principal</td><td class="hoje"></td><td class="amanha"></td><td class="alem"></td><td class="depois"></td></tr>' +
                '<tr class="extra"><td class="type">extra</td><td class="hoje"></td><td class="amanha"></td><td class="alem"></td><td class="depois"></td></tr>' +
                '<tr class="vegetariano"><td class="type">vegeriano</td><td class="hoje"></td><td class="amanha"></td><td class="alem"></td><td class="depois"></td></tr></table>')
        //}
        $($('.ementa .cantina .info')[2]).html(' ')
            .append('<table><tr class="principal"><td rowspan="3" class="title rotate-90">Almoço</td><td class="type">sopa</td><td class="hoje"></td><td class="amanha"></td><td class="alem"></td><td class="depois"></td></tr>' +
                '<tr class="extra"><td class="type">carne</td><td class="hoje"></td><td class="amanha"></td><td class="alem"></td><td class="depois"></td></tr>' +
                '<tr class="vegetariano"><td class="type">peixe</td><td class="hoje"></td><td class="amanha"></td><td class="alem"></td><td class="depois"></td></tr></table>')
            .append('<table class="week"><tr><td class="title">&nbsp;</td><td class="type">&nbsp;</td><td class="hoje">hoje</td><td class="amanha">amanhã</td><td class="alem">quarta</td><td class="depois">quinta</td></tr></table>')
            .append('<table><tr class="principal"><td rowspan="3" class="title rotate-90">Jantar</td><td class="type">sopa</td><td class="hoje"></td><td class="amanha"></td><td class="alem"></td><td class="depois"></td></tr>' +
                '<tr class="extra"><td class="type">carne</td><td class="hoje"></td><td class="amanha"></td><td class="alem"></td><td class="depois"></td></tr>' +
                '<tr class="vegetariano"><td class="type">peixe</td><td class="hoje"></td><td class="amanha"></td><td class="alem"></td><td class="depois"></td></tr></table>')

        this.refresh();
    };


    this.hide = function() {
        for(i=0;i<3;i++) {
            $('#c0 .info').css('opacity', 0);
            $('#c1 .info').css('opacity', 0);
            $('#c2 .info').css('opacity', 0);
            $('#c' + i).css({display:'none'});
        }
    };


    this.open = function() {
        menuLocation = 'ementa';
        dragPos = 1999;
        this.father.hide();
        app.saver.green();
        for(i=0;i<3;i++) {
            $('#c' + i).css({display:'none'});
        }
        setTimeout(function(t){
            for(i=0;i<3;i++) {
                $('#c' + i).css({display:'block',left: t.formula(t,i)});
            }
        },10,this);
        setTimeout(function(id){
            app.setSubtitle('');
            console.log('1000 ' + id);
            app.dots[1].child.move(app.dots[1].child.default);
        },1000,this.default);
        app.now = new Date().getTime();
    };

    this.close = function() {

    };

    this.updatePos = function() {
        return false;
    };

    this.move = function(i) {
        app.now = new Date().getTime();
        $('#c0').css('left', this.formula(i)[0]);
        $('#c1').css('left', this.formula(i)[1]);
        $('#c2').css('left', this.formula(i)[2]);

        if(i==0) {
            $('.ementa')[0].element.father.setSubtitle('Santiago');
            $('#c0 .info').css('opacity', 1);
            $('#c1 .info').css('opacity', 0);
            $('#c2 .info').css('opacity', 0);
        }
        if(i==1) {
            $('.ementa')[0].element.father.setSubtitle('Crasto');
            $('#c0 .info').css('opacity', 0);
            $('#c1 .info').css('opacity', 1);
            $('#c2 .info').css('opacity', 0);
        }
        if(i==2) {
            $('.ementa')[0].element.father.setSubtitle('Snack-Bar');

            $('#c0 .info').css('opacity', 0);
            $('#c1 .info').css('opacity', 0);
            $('#c2 .info').css('opacity', 1);
        }
    };

    this.formula = function(i) {
        switch (i) {
            case 0: return [300,760,1220];
            case 1: return [-100,700,1220];
            case 2: return [-100,295,1095];
        }
    };

    this.refresh = function() {
        $.ajax({
            url: "./backoffice/ementa.php"
        })
            .done(function (data) {

                console.log(data);

                //data = jQuery.parseJSON(data);

                var verify = {santiago:false,crasto:false,snack:false};
                var today = parseInt(data.menus.menu[0]['@attributes'].weekdayNr);
                ementas = {
                    santiago:[
                        {
                            almoco:[],
                            jantar:[]
                        },
                        {
                            almoco:[],
                            jantar:[]
                        },
                        {
                            almoco:[],
                            jantar:[]
                        },
                        {
                            almoco:[],
                            jantar:[]
                        }
                    ],
                    crasto:[
                        {
                            almoco:[],
                            jantar:[]
                        },
                        {
                            almoco:[],
                            jantar:[]
                        },
                        {
                            almoco:[],
                            jantar:[]
                        },
                        {
                            almoco:[],
                            jantar:[]
                        }
                    ],
                    snack:[
                        {
                            almoco:[],
                            jantar:[]
                        },
                        {
                            almoco:[],
                            jantar:[]
                        },
                        {
                            almoco:[],
                            jantar:[]
                        },
                        {
                            almoco:[],
                            jantar:[]
                        }
                    ],
                    dias: ['hoje','amanhã','','']
                };

                //console.log('today: ' + today);
                //console.log('today: ' + today);
                var dia_count=0;
                var aux = data.menus.menu[0]['@attributes'].weekdayNr;
                for(var i = 0;i<data.menus.menu.length && dia_count<4;i++) {
                    if(data.menus.menu[i]['@attributes'].weekdayNr != aux) {
                        aux = data.menus.menu[i]['@attributes'].weekdayNr;
                        dia_count++;
                    }
                    //console.log('ementas');
                    var cantina = '', refeicao = '', pratos = ['','',''];
                    switch (data.menus.menu[i]['@attributes'].canteen) {
                        case 'Refeitório de Santiago':
                            cantina = 'santiago';
                            break;
                        case 'Refeitório do Crasto':
                            cantina = 'crasto';
                            break;
                        case 'Snack-Bar/Self':
                            cantina = 'snack';
                            break;
                    }
                    switch (data.menus.menu[i]['@attributes'].meal) {
                        case 'Almoço':
                            refeicao = 'almoco';
                            break;
                        case 'Jantar':
                            refeicao = 'jantar';
                            break;
                    }
                    if (data.menus.menu[i]['@attributes'].disabled == '0') {

                        if(cantina == 'santiago') {
                            verify.santiago = true;
                        }
                        if(cantina == 'crasto') {
                            verify.crasto = true;
                        }
                        if(cantina == 'snack') {
                            verify.snack = true;
                        }
                        /*
                        pratos[0] =
                            ((typeof data.menus.menu[i].items.item[1]) === 'string') ? data.menus.menu[i].items.item[1] :
                                (((typeof data.menus.menu[i].items.item[2]) === 'string') ? data.menus.menu[i].items.item[2] : '');
                        pratos[1] = ((typeof data.menus.menu[i].items.item[5]) === 'string') ? data.menus.menu[i].items.item[5] : '';
                        pratos[2] = ((typeof data.menus.menu[i].items.item[4]) === 'string') ? data.menus.menu[i].items.item[4] : '';
                        */
                        if(cantina == 'santiago' || cantina == 'crasto') {
                            pratos = [
                                ((typeof data.menus.menu[i].items.item[1]) === 'string') ? data.menus.menu[i].items.item[1] :
                                    (((typeof data.menus.menu[i].items.item[2]) === 'string') ? data.menus.menu[i].items.item[2] : ''),
                                ((typeof data.menus.menu[i].items.item[5]) === 'string') ? data.menus.menu[i].items.item[5] : '',
                                ((typeof data.menus.menu[i].items.item[4]) === 'string') ? data.menus.menu[i].items.item[4] : ''
                            ];
                        } else {
                            pratos = [
                                ((typeof data.menus.menu[i].items.item[0]) === 'string') ? data.menus.menu[i].items.item[0] : '',
                                ((typeof data.menus.menu[i].items.item[1]) === 'string') ? data.menus.menu[i].items.item[1] : '',
                                ((typeof data.menus.menu[i].items.item[2]) === 'string') ? data.menus.menu[i].items.item[2] : ''
                            ];
                        }

                    }

                    console.log(pratos);
                    if(dia_count < 4) {
                        ementas[cantina][dia_count][refeicao] = pratos;
                    }


                }
                if(!verify.santiago) {
                    $($('.ementa .cantina .info')[0]).append('<span id="sem_info">Sem informações</span>');
                }
                if(!verify.crasto) {
                    $($('.ementa .cantina .info')[1]).append('<span id="sem_info">Sem informações</span>');
                }
                if(!verify.snack) {
                    $($('.ementa .cantina .info')[2]).append('<span id="sem_info">Sem informações</span>');
                }
                console.log(verify);
                console.log(data.menus.menu[0]['@attributes'].weekdayNr);
                switch (parseInt(data.menus.menu[0]['@attributes'].weekdayNr)) {
                    case 0: ementas.dias=['hoje','amanhã','terça','quarta'];break;
                    case 1: ementas.dias=['hoje','amanhã','quarta','quinta'];break;
                    case 2: ementas.dias=['hoje','amanhã','quinta','sexta'];break;
                    case 3: ementas.dias=['hoje','amanhã','sexta','sábado'];break;
                    case 4: ementas.dias=['hoje','amanhã','sábado','domingo'];break;
                    case 5: ementas.dias=['hoje','amanhã','domingo','segunda'];break;
                    case 6: ementas.dias=['hoje','amanhã','segunda','terça'];break;
                    case 7: ementas.dias=['hoje','amanhã','terça','quarta'];break;
                }
                $($('#c0 .info table .principal .hoje')[0]).text(dim(ementas.santiago[0].almoco[0]));
                $($('#c0 .info table .principal .amanha')[0]).text(dim(ementas.santiago[1].almoco[0]));
                $($('#c0 .info table .principal .alem')[0]).text(dim(ementas.santiago[2].almoco[0]));
                $($('#c0 .info table .principal .depois')[0]).text(dim(ementas.santiago[3].almoco[0]));
                $($('#c0 .info table .extra .hoje')[0]).text(dim(ementas.santiago[0].almoco[1]));
                $($('#c0 .info table .extra .amanha')[0]).text(dim(ementas.santiago[1].almoco[1]));
                $($('#c0 .info table .extra .alem')[0]).text(dim(ementas.santiago[2].almoco[1]));
                $($('#c0 .info table .extra .depois')[0]).text(dim(ementas.santiago[3].almoco[1]));
                $($('#c0 .info table .vegetariano .hoje')[0]).text(dim(ementas.santiago[0].almoco[2]));
                $($('#c0 .info table .vegetariano .amanha')[0]).text(dim(ementas.santiago[1].almoco[2]));
                $($('#c0 .info table .vegetariano .alem')[0]).text(dim(ementas.santiago[2].almoco[2]));
                $($('#c0 .info table .vegetariano .depois')[0]).text(dim(ementas.santiago[3].almoco[2]));
                $($('#c0 .info table .principal .hoje')[1]).text(dim(ementas.santiago[0].jantar[0]));
                $($('#c0 .info table .principal .amanha')[1]).text(dim(ementas.santiago[1].jantar[0]));
                $($('#c0 .info table .principal .alem')[1]).text(dim(ementas.santiago[2].jantar[0]));
                $($('#c0 .info table .principal .depois')[1]).text(dim(ementas.santiago[3].jantar[0]));
                $($('#c0 .info table .extra .hoje')[1]).text(dim(ementas.santiago[0].jantar[1]));
                $($('#c0 .info table .extra .amanha')[1]).text(dim(ementas.santiago[1].jantar[1]));
                $($('#c0 .info table .extra .alem')[1]).text(dim(ementas.santiago[2].jantar[1]));
                $($('#c0 .info table .extra .depois')[1]).text(dim(ementas.santiago[3].jantar[1]));
                $($('#c0 .info table .vegetariano .hoje')[1]).text(dim(ementas.santiago[0].jantar[2]));
                $($('#c0 .info table .vegetariano .amanha')[1]).text(dim(ementas.santiago[1].jantar[2]));
                $($('#c0 .info table .vegetariano .alem')[1]).text(dim(ementas.santiago[2].jantar[2]));
                $($('#c0 .info table .vegetariano .depois')[1]).text(dim(ementas.santiago[3].jantar[2]));

                $($('#c1 .info table .principal .hoje')[0]).text(dim(ementas.crasto[0].almoco[0]));
                $($('#c1 .info table .principal .amanha')[0]).text(dim(ementas.crasto[1].almoco[0]));
                $($('#c1 .info table .principal .alem')[0]).text(dim(ementas.crasto[2].almoco[0]));
                $($('#c1 .info table .principal .depois')[0]).text(dim(ementas.crasto[3].almoco[0]));
                $($('#c1 .info table .extra .hoje')[0]).text(dim(ementas.crasto[0].almoco[1]));
                $($('#c1 .info table .extra .amanha')[0]).text(dim(ementas.crasto[1].almoco[1]));
                $($('#c1 .info table .extra .alem')[0]).text(dim(ementas.crasto[2].almoco[1]));
                $($('#c1 .info table .extra .depois')[0]).text(dim(ementas.crasto[3].almoco[1]));
                $($('#c1 .info table .vegetariano .hoje')[0]).text(dim(ementas.crasto[0].almoco[2]));
                $($('#c1 .info table .vegetariano .amanha')[0]).text(dim(ementas.crasto[1].almoco[2]));
                $($('#c1 .info table .vegetariano .alem')[0]).text(dim(ementas.crasto[2].almoco[2]));
                $($('#c1 .info table .vegetariano .depois')[0]).text(dim(ementas.crasto[3].almoco[2]));
                $($('#c1 .info table .principal .hoje')[1]).text(dim(ementas.crasto[0].jantar[0]));
                $($('#c1 .info table .principal .amanha')[1]).text(dim(ementas.crasto[1].jantar[0]));
                $($('#c1 .info table .principal .alem')[1]).text(dim(ementas.crasto[2].jantar[0]));
                $($('#c1 .info table .principal .depois')[1]).text(dim(ementas.crasto[3].jantar[0]));
                $($('#c1 .info table .extra .hoje')[1]).text(dim(ementas.crasto[0].jantar[1]));
                $($('#c1 .info table .extra .amanha')[1]).text(dim(ementas.crasto[1].jantar[1]));
                $($('#c1 .info table .extra .alem')[1]).text(dim(ementas.crasto[2].jantar[1]));
                $($('#c1 .info table .extra .depois')[1]).text(dim(ementas.crasto[3].jantar[1]));
                $($('#c1 .info table .vegetariano .hoje')[1]).text(dim(ementas.crasto[0].jantar[2]));
                $($('#c1 .info table .vegetariano .amanha')[1]).text(dim(ementas.crasto[1].jantar[2]));
                $($('#c1 .info table .vegetariano .alem')[1]).text(dim(ementas.crasto[2].jantar[2]));
                $($('#c1 .info table .vegetariano .depois')[1]).text(dim(ementas.crasto[3].jantar[2]));

                $($('#c2 .info table .principal .hoje')[0]).text(dim(ementas.snack[0].almoco[0]));
                $($('#c2 .info table .principal .amanha')[0]).text(dim(ementas.snack[1].almoco[0]));
                $($('#c2 .info table .principal .alem')[0]).text(dim(ementas.snack[2].almoco[0]));
                $($('#c2 .info table .principal .depois')[0]).text(dim(ementas.snack[3].almoco[0]));
                $($('#c2 .info table .extra .hoje')[0]).text(dim(ementas.snack[0].almoco[1]));
                $($('#c2 .info table .extra .amanha')[0]).text(dim(ementas.snack[1].almoco[1]));
                $($('#c2 .info table .extra .alem')[0]).text(dim(ementas.snack[2].almoco[1]));
                $($('#c2 .info table .extra .depois')[0]).text(dim(ementas.snack[3].almoco[1]));
                $($('#c2 .info table .vegetariano .hoje')[0]).text(dim(ementas.snack[0].almoco[2]));
                $($('#c2 .info table .vegetariano .amanha')[0]).text(dim(ementas.snack[1].almoco[2]));
                $($('#c2 .info table .vegetariano .alem')[0]).text(dim(ementas.snack[2].almoco[2]));
                $($('#c2 .info table .vegetariano .depois')[0]).text(dim(ementas.snack[3].almoco[2]));
                $($('#c2 .info table .principal .hoje')[1]).text(dim(ementas.snack[0].jantar[0]));
                $($('#c2 .info table .principal .amanha')[1]).text(dim(ementas.snack[1].jantar[0]));
                $($('#c2 .info table .principal .alem')[1]).text(dim(ementas.snack[2].jantar[0]));
                $($('#c2 .info table .principal .depois')[1]).text(dim(ementas.snack[3].jantar[0]));
                $($('#c2 .info table .extra .hoje')[1]).text(dim(ementas.snack[0].jantar[1]));
                $($('#c2 .info table .extra .amanha')[1]).text(dim(ementas.snack[1].jantar[1]));
                $($('#c2 .info table .extra .alem')[1]).text(dim(ementas.snack[2].jantar[1]));
                $($('#c2 .info table .extra .depois')[1]).text(dim(ementas.snack[3].jantar[1]));
                $($('#c2 .info table .vegetariano .hoje')[1]).text(dim(ementas.snack[0].jantar[2]));
                $($('#c2 .info table .vegetariano .amanha')[1]).text(dim(ementas.snack[1].jantar[2]));
                $($('#c2 .info table .vegetariano .alem')[1]).text(dim(ementas.snack[2].jantar[2]));
                $($('#c2 .info table .vegetariano .depois')[1]).text(dim(ementas.snack[3].jantar[2]));

                $('#c0 .info table.week .alem').text(ementas.dias[2]);
                $('#c0 .info table.week .depois').text(ementas.dias[3]);
                $('#c1 .info table.week .alem').text(ementas.dias[2]);
                $('#c1 .info table.week .depois').text(ementas.dias[3]);
                $('#c2 .info table.week .alem').text(ementas.dias[2]);
                $('#c2 .info table.week .depois').text(ementas.dias[3]);

                if(verify['santiago']) {
                    setTimeout(function() {
                        /*
                         app.dots[1].child.move(1);

                         $('.ementa')[0].element.father.setSubtitle('Crasto');

                         $('#c0 .info').css('opacity', 0);
                         $('#c1 .info').css('opacity', 1);
                         $('#c2 .info').css('opacity', 0);
                         */
                        console.log('verify canteen');
                    },1000);
                }
                function dim (str) {
                    try {
                        return (str.length > 20) ? str.substr(0, 20) + '...' : str;
                    } catch(e) {
                        //console.log('ementas error');
                    }
                };

            })
            .error(function(){
                //console.log("error getting data");
            });

        $.ajax({
            url: "./backoffice/api.php?cantina=1"
        })
            .done(function (data) {
                console.log(data);
                app.dots[1].child.default = parseInt(data['default'])-1;
            });
    };

    this.init();

}

function meal_ignore(str) {
    var ignore = ['Pão','salada','fruta','doce'];
    var regex = ignore[0];
    for (var i = 0; i < ignore.length;i++) {
        regex += '|' + ignore;
    }
    regex = '/' + regex + '/';
    return str.search(regex);
}