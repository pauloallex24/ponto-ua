function saver(father) {
    this.father = father;
    this.state = 0;

    this.init = function() {
        $('body').append('<div class="saver" onclick="app.saver.click()"></div>');
        $('.saver').append('<div class="gif" style="left:-500px;"></div>');
        setTimeout(function(){
            app.saver.open();
        },100);
    };


    this.hide = function() {

    };

    this.open = function() {
        this.state = 0;
        app.now = new Date().getTime();
        menuLocation = 'saver';
        dragPos = 1999;
        app.saver.white();
        this.father.hide();
        $('.qr').css({'background-image':'url(assets/img/qr.svg)',heigth:'50px !important',width:50,top:905,left:75});
        $('.qr').addClass('qrclose');
        $('.qr .title').css({left:-1000});
        $('.qr .go').css({left:-1000});
        $('.saver').css({left:0});
        setTimeout(function(){
            $('.saver .gif').css({left:800});
        },100);
    };

    this.openqr = function() {
        this.state = 1;
        app.close();
        app.changeQrUrl('000000','ffffff',token);
        app.now = new Date().getTime();
        $('.saver').css({left:0});
        $('.qr').css({'background-image':'url(' + app.qrurl + ')',height:'300px !important',width:300,top: 400,left:850});
        $('.qr').removeClass('qrclose');
        $('.qr .title').css({left:450});
        $('.qr .go').css({left:700});

        setTimeout(function(){
            $('.saver .gif').css({left:-500});
        },100);

    };

    this.close = function() {
        this.state = 2;
        app.now = new Date().getTime();
        this.father.hide();
        dragPos = 1999;
        $('.qr').addClass('qrclose');
        $('.qr').css({'background-image':'url(assets/img/qr.svg)',heigth:'50px !important',width:50,top:905,left:75});
        $('.qr .title').css({left:-1000});
        $('.qr .go').css({left:-1000});
        setTimeout(function(){
            $('.saver .gif').css({left:-500});
            $('.saver').css({left:-1920});
        },100);
    };

    this.green = function () {
        $('.qr').css({'background-image':'url(assets/img/qr_green.svg)'});
    };
    this.white = function () {
        $('.qr').css({'background-image':'url(assets/img/qr.svg)'});
    };

    this.click = function() {
        if(this.state == 1) {
            this.close();
        }
        if(this.state == 0) {
            this.openqr();
        }

    };

    this.updatePos = function() {

    };

    this.formula = function(e,i) {

    };

    this.move = function (index,obj) {

    };

    this.refresh = function() {

    };

    this.init();
}