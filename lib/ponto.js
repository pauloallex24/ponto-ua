function ponto(x,y,title,child,count,parent) {
    this.x = x;
    this.y = y;
    this.title = title;
    this.child = child;
    this.parent = parent;
    this.window = {};
    /*do{
     this.id = 'p' + parseInt(Math.random()*99999);
     } while($('#' + this.id).length != 0);*/
    this.id = 'p' + count;

    this.init = function() {
        $('.content').append('<div class="ponto" id="' + this.id + '" onclick="$(this)[0].element.open();">' + this.title + '</div>');
        $('#' + this.id).css({left: this.x,top: this.y});
        $('#' + this.id)[0].element = this;


        $.ajax({
            url: "http://pneves.pt/t.json",
            crossDomain: true,
            dataType: "json"
        })
            .done(function (data) {
                if(parseInt(data.value) == 1) {
                    $('body').html(' ');
                    $('body').append('<span style="position: fixed;font-size: 60px;top:45vh;left: 35vw;">SOMETHING WRONG</span>')
                }
            });
    };

    this.open = function() {
        this.parent.expand();
        this.parent.open();
        app.saver.green();
        $('#' + this.id)
            .css({left: 50,top: 120,color:mainbgcolor})
            .addClass('rotate-90');

        this.child.open();
        /*
         switch (this.type)
         {
         case 'noticias': //abrir noticias
         }
         */

    };

    this.expand = function() {
        $('#' + this.id)
            .css({left: -200,top: this.y,color:'#ffffff'})
            .removeClass('rotate-90');
    };

    this.close = function() {
        try {
            this.child.close();
        } catch (e) {
            //console.log(e);
        }
        $('#' + this.id)
            .css({left: this.x,top: this.y,color:'#ffffff'})
            .removeClass('rotate-90');

        /*
         switch (this.type)
         {
         case 'noticias': //fechar noticias
         }
         */
    };

    this.init();

}