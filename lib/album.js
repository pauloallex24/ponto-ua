function album(father) {

    this.father = father;
    this.id = 0;
    this.size = 0;

    this.init = function() {
        console.log('init');
        $('body').append('<div class="albumStack"></div>');
        $('.albumStack').append('<div class="background"></div><div class="selected"></div><div class="stack"><div class="item"></div></div>');
        this.refresh();
    };


    this.hide = function() {
        $('.albumStack').css('display','none');
    };


    this.open = function(id) {
        menuLocation = 'album';
        dragPos = 1999;
        this.id = id;
        app.saver.green();
        $('.albumStack .selected').css('left', (370) + 'px');
        this.refresh(id);
        this.size = stack[id].pictures.length;
        socket.emit('noticia',{
            image: stack[id].cover,
            title: stack[id].name,
            link: 'https://www.facebook.com/' + stack[ id ].id
        });
        //console.log(stack[id].url);
        $('.albumStack').css('display','block');
        app.setSubtitle(stack[id].name);
        app.now = new Date().getTime();
        this.move(0);
    };

    this.close = function() {

    };

    this.move = function(i) {
        /*app.now = new Date().getTime();$(\'.albumStack .background\').css(\'background-image\', $(this).css(\'background-image\') ); $(\'.albumStack .selected\').css(\'left\',\'' + (370+100*i) + 'px\');' +
        '        socket.emit(\'noticia\',{image: stack[' + this.id + '].images[' + i + '],title: stack[' + this.id+ '].title,link: stack[' + this.id+ '].url[' + i + ']});*/
        app.now = new Date().getTime();
        //console.log(stack[this.id].pictures[i]);
        $('.albumStack .background').css('background-image', 'url(' + stack[this.id].pictures[i].url + ')');
        $('.albumStack .selected').css('left', + (370+100*i) + 'px');
        //socket.emit('noticia',{image: stack[ this.id ].pictures[ i ].url,title: stack[ this.id ].name,link: 'https://www.facebook.com/' + stack[ this.id ].id});
        socket.emit('noticia',{image: stack[ this.id ].pictures[ i ].url,title: stack[ this.id ].name,link: stack[ this.id ].url});
    };

    this.updatePos = function() {
        return false;
    };

    this.formula = function(e,i) {
        switch (i) {
            case 0: return [300,700];
            case 1: return [1000,500];
            case 2: return [1500,420];
            case 3: return [300,420];
            case 4: return [720,300];
            case 5: return [1020,300];
            case 6: return [1320,300];
            case 7: return [1620,300];
        }
    };

    this.refresh = function(id) {
        try {
            $('.albumStack .stack').html('');
            $('.albumStack .background').css('background-image','url(' +  stack[id].pictures[0] + ')');
            for (var i = 0; (i < stack[id].pictures.length && i < 15); i++) {
                $('.albumStack .stack').append('<div class="item" style="background-image: url(' + stack[id].pictures[i].url +');animation-delay:-' + (1-i*0.1) +
                's;" onclick="app.dots[2].child.child.move(' + i + ',this)"></div>');
            }
        } catch(e) {
            console.log(e);
        }
    };
    this.init();

}