function pontoua() {
    this.dots = [];
    this.count = 0;
    this.touch = {x:1920,y:0};
    this.offset = {x:0,y:0};
    this.qrurl = '';
    this.now = new Date().getTime();
    this.timeout = setInterval(function(){
        if((new Date().getTime()) - app.now > 180000) {
            console.log('timeout x');
            $('.logo')[0].element.close();
            app.saver.open();
        }
    },10000);

    this.init = function () {
        $('body').append('<div class="qr animation qrclose" onclick="app.saver.openqr()"><div class="title" onclick="setTimeout(function(){app.saver.close()},20);">Pretende ligar-se através do seu dispositivo movél? Use este QR Code. :)</div><div class="go" onclick="setTimeout(function(){app.saver.close()},20);"> <img src="./assets/img/finger.png" alt="" style="width:30px;height: auto;">&nbsp;&nbsp;avançar</div></div>');
        $('body').append('<div class="content touch"></div>');
        $('body').append('<div class="subtitle"><div class="one" style="opacity: 0;">one</div><div class="two" style="opacity: 0;">two</div></div>');
        $('body').append('<div class="logo" onclick="$(this)[0].element.close()"> <div class="green"></div></div>');
        $('.logo')[0].element = this;
    };

    this.close = function () {
        socket.emit('menu');
        app.saver.white();
        menuLocation = 'menu';
        for (var i = 0; i < this.dots.length; i++) {
            this.dots[i].close();
            console.log("pontoua close");
        }
        $('.content').css({left:0,'background-color':mainbgcolor});
        $('.subtitle .one').css('opacity',0);
        $('.subtitle .two').css('opacity',0);
        $('.logo')
            .css({left:880,top:430})
            .removeClass('rotate90');
        color = "000000";
        bgcolor = "B9FF43";
        app.changeQrUrl(color,bgcolor,token);
        $('.logo .green').css({opacity:0});
    };

    this.hide = function () {
        for (var i = 0; i < this.dots.length; i++) {
            try {
                this.dots[i].child.hide();
            } catch (e) {
                console.log('smething went wrong');
            }
        }
    };

    this.expand = function() {
        for (var i = 0; i < this.dots.length; i++) {
            this.dots[i].expand();
        }
        $('.content').css({left:0,'background-color':'#ffffff'});

        $.ajax({
            url: "./backoffice/api.php?updatealbuns=1"
        })
            .done(function (data) {
                console.log(data['updatealbuns']);
            });
    };

    this.open = function () {
        $('.content').css({left:-1620,'background-color':'#ffffff'});
        $('.logo')
            .css({left:0})
            .addClass('rotate90');
        $('.logo .green').css({opacity:1});
        color = "000000";
        bgcolor = "FFFFFF";
        app.changeQrUrl(color,bgcolor,token);
    };

    this.add = function(x,y,title,type) {
        this.count++;
        this.dots.push(new ponto(x,y,title,type,this.count,this));
    };

    this.setSubtitle = function (txt) {
        if($('.subtitle .one').css('opacity')==1) {
            $('.subtitle .two').text(txt).css('opacity',1);
            $('.subtitle .one').css('opacity',0);
        } else {
            $('.subtitle .one').text(txt).css('opacity',1);
            $('.subtitle .two').css('opacity',0);
        }
    };
    this.changeQr = function(color,bgcolor,url) {

        this.qrurl = 'http://api.qrserver.com/v1/create-qr-code/?size=250x250&color=' + color + '&bgcolor=' + bgcolor + '&data=' + socket_domain + ((socket_server_port!=80)?':'+socket_server_port:'') + '/controller/' + url;
        $('.qr').css('background-image','url(' + this.qrurl + ')');
    };

    this.changeQrUrl = function(color,bgcolor,url) {

        this.qrurl = 'http://api.qrserver.com/v1/create-qr-code/?size=250x250&color=' + color + '&bgcolor=' + bgcolor + '&data=' + socket_domain + ((socket_server_port!=80)?':'+socket_server_port:'') + '/controller/' + url;
    };

    /*this.touchStart = function() {
        $('*').on( "touchmove", function(e) {e.preventDefault();});
        $('*').on( "mousemove", function(e) {e.preventDefault();});
        $('.touch').on( "touchstart", function(e) {
            console.log('I');
            this.offset = {x:e.originalEvent.touches[0].pageX,y:e.originalEvent.touches[0].pageY};*/
            /*$(e.delegateTarget).removeClass('animation');

             e.delegateTarget.pontoua.offset = {x:e.originalEvent.touches[0].pageX - e.delegateTarget.pontoua.getPos().x,
             y:e.originalEvent.touches[0].pageY - e.delegateTarget.pontoua.getPos().y-120};

             e.delegateTarget.pontoua.initial = {x: e.originalEvent.touches[0].pageX - 960,
             y:  e.originalEvent.touches[0].pageY - 540};

             e.delegateTarget.pontoua.initial.end = {x:0,y:0};*/
        /*});
        $('.touch').on( "touchmove", function(e) {
            console.log('.');
            this.offset = {x:e.originalEvent.touches[0].pageX,y:e.originalEvent.touches[0].pageY};
            if((this.touch.x + e.originalEvent.touches[0].pageX - this.offset.x) > 0 || (this.touch.x + e.originalEvent.touches[0].pageX - this.offset.x) <1920) {
                this.touch.x = this.touch.x + e.originalEvent.touches[0].pageX - this.offset.x;
            } else {
                if((this.touch.x + e.originalEvent.touches[0].pageX - this.offset.x) < 0){
                    this.touch.x = 0;
                } else if((this.touch.y + e.originalEvent.touches[0].pageY - this.offset.y) > 1920){
                    this.touch.x = 1920;
                }
            }*/
            /*if((this.touch.y + e.originalEvent.touches[0].pageY - this.offset.y) > 0 || (this.touch.y + e.originalEvent.touches[0].pageY - this.offset.y) <1920) {
                this.touch.y = this.touch.y + e.originalEvent.touches[0].pageY - this.offset.y;
            } else {
                if((this.touch.y + e.originalEvent.touches[0].pageY - this.offset.y) < 0){
                    this.touch.y = 0;
                } else if((this.touch.y + e.originalEvent.touches[0].pageY - this.offset.y) > 1920){
                    this.touch.y = 1920;
                }
            }*/
            /*e.delegateTarget.pontoua.x = e.delegateTarget.pontoua.x + (e.originalEvent.touches[0].pageX - e.delegateTarget.pontoua.getPos().x - e.delegateTarget.pontoua.offset.x);//(e.delegateTarget.pontoua.parent.x - e.delegateTarget.pontoua.x ) - e.originalEvent.touches[0].pageX;
             e.delegateTarget.pontoua.y = (e.delegateTarget.pontoua.y + (e.originalEvent.touches[0].pageY - e.delegateTarget.pontoua.getPos().y - e.delegateTarget.pontoua.offset.y)-120);//(e.delegateTarget.pontoua.parent.y - e.delegateTarget.pontoua.y ) - e.originalEvent.touches[0].pageY;
             e.delegateTarget.pontoua.update();
             e.preventDefault();
             e.delegateTarget.pontoua.initial.end = {x:e.originalEvent.touches[0].pageX,y:e.originalEvent.touches[0].pageY};*/
        /*});

        $('.touch').on( "touchend", function(e) {
            console.log('E');*/
            /*
             $(e.delegateTarget).addClass('animation');
             e.delegateTarget.pontoua.offset = undefined;
             var initial = {x: e.delegateTarget.pontoua.initial.end.x - 960,
             y:  e.delegateTarget.pontoua.initial.end.y - 540};
             if(initial.x<e.delegateTarget.pontoua.initial.x && initial.y>e.delegateTarget.pontoua.initial.y) {
             if(e.delegateTarget.pontoua.name == 'Dot') {
             e.delegateTarget.pontoua.active = false;
             e.delegateTarget.pontoua.toCorner();
             setTimeout(function(e){e.delegateTarget.pontoua.showChilds();},500,e);
             }else if(e.delegateTarget.pontoua.name == 'Father') {
             e.delegateTarget.pontoua.active = false;
             e.delegateTarget.pontoua.toCorner();
             }
             }
             else if (Math.abs(initial.x)+Math.abs(initial.y) < Math.abs(e.delegateTarget.pontoua.initial.x) + Math.abs(e.delegateTarget.pontoua.initial.y)) {
             if(e.delegateTarget.pontoua.name == 'Dot') {
             e.delegateTarget.pontoua.active = false;
             elements.father.hideChilds();
             e.delegateTarget.pontoua.toCorner();

             }
             else if(e.delegateTarget.pontoua.name == 'Father') {
             e.delegateTarget.pontoua.active = false;
             e.delegateTarget.pontoua.toCenter();
             } else {
             }
             } else {
             if(e.delegateTarget.pontoua.name == 'Dot') {
             e.delegateTarget.pontoua.parent.active = false;
             e.delegateTarget.pontoua.parent.toCorner();
             } else if(e.delegateTarget.pontoua.name == 'Father') {
             e.delegateTarget.pontoua.toCenter();
             }
             }
             e.delegateTarget.pontoua.initial = undefined;*/
        /*});

    };*/

    //this.touchStart();
    this.init();
}