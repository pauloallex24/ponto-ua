var config = require('./config.js').config;

if(!config.debug) {
    console.log = function() {};
}

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var connections = [];
pannel = false;
token = [randomString(5),randomString(5),randomString(5),randomString(5),randomString(5),randomString(5),randomString(5),randomString(5),randomString(5),randomString(5)];
tokenCount = 1;
console.log(token[0]);

app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', "http://"+req.headers.host+':8000');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        next();
    }
);

app.get('/', function(req, res){
    res.sendFile(__dirname + '/404.html');
});
app.get('/controller', function(req, res){
    res.sendFile(__dirname + '/error.html');
});
app.get('/controller/:token', function(req, res){
    tokenCount = (++tokenCount % 10);
    if(token.indexOf(req.params.token)>=0) {
        token[token.indexOf((req.params.token)+9 %10)] = randomString(5);
        try{pannel.emit('token', token[tokenCount])}catch(e){};
        res.sendFile(__dirname + '/controller.html');
    }
    else {
        //io.emit('token', token[tokenCount]);
        res.sendFile(__dirname + '/error.html');
    }
});
app.get('/logo/', function(req, res){
    res.sendFile(__dirname + '/assets/img/ponto.ua.png');
});
app.get('/socketio/', function(req, res){
    res.sendFile(__dirname + '/assets/script/socket.io.js');
});

io.on('connection', function(socket){
    console.log('connected');
    socket.idcount = false;
    socket.player = '';
    socket.idcount = randomString(5);
    //connections.push(socket);

    socket.on('disconnect', function(){
        /*
        if(socket.idcount) {
            console.log('disconnect ' + socket.idcount);
            io.emit('disconnected', {id: socket.idcount});
            console.log(socket.idcount);

            for(i=0;i<connections.length;i++) {
                if(connections[i].idcount == socket.idcount) {
                    connections.splice(i,1);
                }
            }
        }*/
    });
    socket.on('left', function(){
        console.log('left');
        //if(socket.idcount == connections[0].idcount) {
            io.emit('left');
        //}
    });
    socket.on('right', function(){
        console.log('right');
        //if(socket.idcount == connections[0].idcount) {
            io.emit('right');
        //}
    });
    socket.on('fire', function(){
        console.log('fire');
        //if(socket.idcount == connections[0].idcount) {
            io.emit('fire');
        //}
    });
    socket.on('pannel', function(msg){
        console.log('new pannel');
        pannel = socket;
        try{pannel.emit('token',token[tokenCount])}catch(e){};
        /*
        for(i=0;i<connections.length;i++) {
            if(connections[i].idcount == socket.idcount) {
                connections.splice(i,1);
            }
        }*/
    });

    socket.on('noticia', function(msg){
        console.log('noticia');
        io.emit('noticia', msg);
    });

    socket.on('menu', function(msg){
        console.log('menu');
        io.emit('menu', msg);
    });
    socket.on('menu_', function(msg){
        console.log('menu_');
        io.emit('menu_', msg);
    });

    socket.on('space', function(){
        io.emit('space');
    });

    socket.on('reload', function(){
        io.emit('reload');
    });

});

setTimeout(function() {
    console.log('emit_init');
    io.emit('init');
},5000);


http.listen(config.port, function(){
    console.log('listening on *: ' + config.port);
});


function randomString(length) {
    characters = 'ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789';
    random = '';
    for (i = 0; i < length; i++) {
        random += characters.charAt([parseInt(Math.random()*(characters.length - 1))]);
    }
    return random;
}