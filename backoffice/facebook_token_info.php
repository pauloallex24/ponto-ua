<?php
#include_once '../config.php';
#include_once '../vendor/php-graph-sdk-5.0.0/src/Facebook/autoload.php';
require_once 'helper.php';
require_once 'mysql.php';
require_once 'class.phpmailer.php';
require_once 'class.smtp.php';

$query = 'SELECT * FROM facebook ORDER BY id DESC LIMIT 1;';
$stmt = $conn->prepare($query);
$stmt->execute();
$token_db = $stmt->fetchAll();
ini_set('session.gc_maxlifetime', 14400);
session_set_cookie_params(14400);

if(!isset($fb)) {
    $fb = new Facebook\Facebook([
        'app_id' => $facebook_api_app_id,
        'app_secret' => $facebook_api_app_secret,
        'default_graph_version' => 'v2.8',
    ]);
}
if (session_status() === PHP_SESSION_NONE){session_start();}
$expires = time() + 60 * 60 * 24 * 60;
$accessToken = new Facebook\Authentication\AccessToken($token_db[0]['accesstoken'],$expires);
$fb->setDefaultAccessToken($accessToken->getValue());
$_SESSION['access_token'] = (string)$accessToken->getValue();
if($accessToken->isExpired()) {
    $query = "SELECT * FROM params WHERE name = 'email' LIMIT 1";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    $param = $stmt->fetchAll();

    $mail = new PHPMailer;
    $mail->IsSMTP();
    $mail->Host = $smtp_server;
    $mail->SMTPAuth = true;
    $mail->Username = $smtp_email;
    $mail->Password = $smtp_password;
    $mail->SMTPSecure = $smtp_security;
    $mail->Port = $smtp_port;
    $mail->CharSet = 'UTF-8';
    $mail->setFrom($smtp_email, 'Ponto.UA');
    $mail->addAddress($param[0]['value']);
    $mail->Subject = 'Ponto.UA - Sessão do Facebook expirada';
    $mail->Body = 'A sessão do Facebook (para aceder aos álbuns) do painel interativo Ponto.UA expirou. <br> Faça login no backoffice
    (em <a href="' . $domain . '/backoffice.php">' . $domain . '/backoffice.php</a> ) para renovar a sessão,
    caso contrário os álbuns do facebook não se manterão atualizados.';
    $mail->AltBody = 'A sessão do Facebook (para aceder aos álbuns) do painel interativo Ponto.UA expirou. Faça login no backoffice
    (em ' . $domain . '/backoffice.php ) para renovar a sessão, caso contrário os álbuns do facebook não se manterão atualizados.';

    if(!$mail->send()) {
        dd('Message could not be sent.');
        dd('Mailer Error: ' . $mail->ErrorInfo);
    }

} else {
    $response = $fb->get('/me');
    $userNode = $response->getGraphUser();

    if($token_db[0]['accesstoken'] != $_SESSION['access_token']) {
        try {
            $stmt = $conn->prepare(
                'INSERT INTO facebook ( facebook_id, name, accesstoken, created_at) VALUES (:facebook_id, :name, :accesstoken, :created_at)'
            );


            $facebook_id = $userNode->getField('id');
            $name = $userNode->getName();
            $created_at = time();

            $stmt->bindValue(':facebook_id', $facebook_id);
            $stmt->bindValue(':name', $name);
            $stmt->bindValue(':accesstoken', $_SESSION['access_token']);
            $stmt->bindValue(':created_at', $created_at);

            if (!$stmt->execute()) {
                echo "PDO Error 1.1:\n";
                print_r($stmt->errorInfo());
                exit;
            }
            unset($stmt);
        } catch (PDOException $e) {
            print "PDO Statement Error!: " . $e->getMessage() . "<br/>";
            exit;
        }
    }
}