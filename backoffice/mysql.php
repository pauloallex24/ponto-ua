<?php
require_once 'helper.php';
require_once __DIR__ . '/../config.php';


try {
    $conn = new PDO(
        'mysql:host=' . $db_host .';dbname=' . $db_name, $db_user, $db_password,
    array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
    );
} catch (PDOException $e) {
    print "PDO Connection Error!: " . $e->getMessage() . "<br/>";
    exit;
}
/*
try {
    $stmt = $conn->prepare(
        'INSERT INTO albums (id, name, cover, created_at) VALUES (:id, :name, :cover, :created_at)'
    );

    $id = 123123123;
    $name = 'Conteudo do post';
    $cover = 'Conteudo do post';
    $created_at = 'Conteudo do post';

    $stmt->bindValue(':id', $id);
    $stmt->bindValue(':name', $name);
    $stmt->bindValue(':cover', $cover);
    $stmt->bindValue(':created_at', $created_at);

    if ( ! $stmt->execute() )
    {
        echo "PDO Error 1.1:\n";
        print_r($stmt->errorInfo());
        exit;
    }
    unset($stmt);
} catch (PDOException $e) {
    print "PDO Statement Error!: " . $e->getMessage() . "<br/>";
    exit;
}
dd("hhhhhh");exit;
*/