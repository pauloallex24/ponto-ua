<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mapa Viwer</title>
    <style>
        div {
            width: 90vw;
            height: calc(90vw / 1.5);
            position: fixed;
            background-image: url(../assets/buildings/mapa.jpg);
            background-size:contain;
            background-repeat: no-repeat;
            background-position: center;
        }
        span {
            width: 80px;
            height: auto;
            position: fixed;
            background-color: #333333;
            border: 3px solid #ddd;
            color: #ffffff;
            font-size: 16px;
            padding: 1px 4px;
            top: <?php echo (90 / 1.5) * ((int)$_GET['y'] / 1080); ?>vw;
            left: <?php echo (90) * (((int)$_GET['x'] - 300) / 1620); ?>vw;
        }
        span:before {
            content: "";
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 30px 10px 0 10px;
            border-color: #ddd transparent transparent transparent;
            position: absolute;
            bottom: -30px;
            left: -3px;
        }
        span:after {
            content: "";
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 20px 6px 0 6px;
            border-color: #333333 transparent transparent transparent;
            position: absolute;
            bottom: -20px;
            left: 0;
        }
        body {
            margin: 0;
        }
    </style>
</head>
<body>
<div></div>
<span><?php echo $_GET['name']; ?></span>
</body>
</html>