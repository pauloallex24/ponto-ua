<?php

function dd($data) {
    echo "<pre>";
    print_r($data);
    echo "<pre>";
}

function dd_url($data) {
    echo "<pre><a href=\"";
    print_r($data);
    echo "\" target=\"_blank\">";
    print_r($data);
    echo"</a><pre>";
}

function ext($str) {
    echo $str;
    exit;
}

function random($length = 32) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}