<?php

require_once 'facebook_token_info.php';
try {
    $album_req = $fb->request('GET', '/universidadedeaveiro/albums');
    $albums_edge = $fb->getClient()->sendRequest($album_req)->getGraphEdge();

    $count = 0;
    /*
        try {
            $stmt = $conn->prepare(
                'SET FOREIGN_KEY_CHECKS = 0; TRUNCATE albums; TRUNCATE pictures; SET FOREIGN_KEY_CHECKS = 1;'
            );
            if ( ! $stmt->execute() )
            {
                echo "PDO Error 1.1:\n";
                print_r($stmt->errorInfo());
                exit;
            }
            unset($stmt);
        } catch (PDOException $e) {
            print "PDO Statement Error!: " . $e->getMessage() . "<br/>";
            exit;
        }*/

    $last_album_id = 0;
    $last_picture_id = 0;

    $query = 'SELECT * FROM albums ORDER BY count_id DESC LIMIT 1;';
    foreach ($conn->query($query) as $row) {
        $last_album_id = $row['count_id'];
    }
    $query = 'SELECT * FROM pictures ORDER BY count_id DESC LIMIT 1;';
    foreach ($conn->query($query) as $row) {
        $last_picture_id = $row['count_id'];
    }


    $excluded_name = [];
    $excluded_id = [];
    $added_albums = [];

    try {

        $query = 'SELECT * FROM excluded_albums;';
        foreach ($conn->query($query) as $row) {

            if($row['album_name'] != "" && $row['album_name'] != null) {
                array_push($excluded_name, $row['album_name']);
            }
            if($row['album_id'] != "" && $row['album_id'] != null) {
                array_push($excluded_id, $row['album_id']);
            }
        }
    } catch (PDOException $e) {
        print "PDO Statement Error!: " . $e->getMessage() . "<br/>";
        exit;
    }

    //while($count<20) {
        $albums = $albums_edge->asArray();



        for($i = 0; $i < sizeof($albums) && $count<23; $i++) {
            $error = false;
            $album_resp = $albums[$i];

            if (!in_array($album_resp['name'],$excluded_name)
                && !in_array($album_resp['id'],$excluded_id)
                && !in_array($album_resp['name'],$added_albums) ) {

                array_push($added_albums,$album_resp['name']);

                try {
                    $photos = $fb->get('/' . $album_resp['id'] . '/photos?fields=id,url,link,images')->getGraphEdge()->asArray();
                } catch (Facebook\Exceptions\FacebookResponseException $e) {
                    $error = true;
                    echo 'Photos Message: ' . $e->getMessage();
                }

                try {
                    $album_req = $fb->request('GET', '/' . $album_resp['id'] . '?fields=id,cover_photo,link,name');
                    $album = $fb->getClient()->sendRequest($album_req)->getGraphNode()->asArray();
                } catch (Facebook\Exceptions\FacebookResponseException $e) {
                    $error = true;
                    echo 'Album Message: ' . $e->getMessage();
                }

                try {
                    $cover_req = $fb->request('GET', '/' . $album['cover_photo']['id'] . '?fields=images');
                    $cover = $fb->getClient()->sendRequest($cover_req)->getGraphNode()->asArray();
                } catch (Facebook\Exceptions\FacebookResponseException $e) {
                    $error = true;
                    echo 'Cover Message: ' . $e->getMessage();
                }

                if (!$error && sizeof($photos) > 0) {
                    $count++;

                    /*
                                    echo "<br><br><br><h1>" . $album_resp['name'] . "</h1><br>";
                                    dd($cover['images'][0]['source']);
                                    dd("CONTADOR: " . $count . "<br>");

                                    dd($album['id'] . ' ' . $album['name']);

                                    for($n = 0; $n < sizeof($photos) && $n < 14; $n++) {
                                        dd($photos[$n]);
                                    }
                                    */
                    try {
                        $stmt = $conn->prepare(
                            'INSERT INTO albums (id, name, cover, url, created_at) VALUES (:id, :name, :cover, :url, :created_at)'
                        );

                        $id = $album_resp['id'];
                        $name = $album['name'];
                        $cover = $cover['images'][0]['source'];
                        $url = $album['link'];
                        $created_at = time();

                        $stmt->bindValue(':id', $id);
                        $stmt->bindValue(':name', $name);
                        $stmt->bindValue(':cover', $cover);
                        $stmt->bindValue(':url', $url);
                        $stmt->bindValue(':created_at', $created_at);

                        if (!$stmt->execute()) {
                            echo "PDO Error 1.1:\n";
                            print_r($stmt->errorInfo());
                            exit;
                        }
                        unset($stmt);
                    } catch (PDOException $e) {
                        print "PDO Statement Error!: " . $e->getMessage() . "<br/>";
                        exit;
                    }

                    for ($n = 0; $n < sizeof($photos) && $n < 14; $n++) {
                        #dd_url($photos[$n]['images'][0]['source']);

                        try {
                            $stmt = $conn->prepare(
                                'INSERT INTO pictures (id, url, albums_id, created_at) VALUES (:id, :url, :albums_id, :created_at)'
                            );


                            $id = (int)$photos[$n]['id'];
                            $url = $photos[$n]['images'][0]['source'];
                            $albums_id = $album_resp['id'];
                            $created_at = time();



                            $stmt->bindValue(':id', $id);
                            $stmt->bindValue(':url', $url);
                            $stmt->bindValue(':albums_id', $albums_id);
                            $stmt->bindValue(':created_at', $created_at);




                            if (!$stmt->execute()) {
                                echo "PDO Error 1.1:\n";
                                print_r($stmt->errorInfo());
                                exit;
                            }
                            unset($stmt);
                        } catch (PDOException $e) {
                            print "PDO Statement Error!: " . $e->getMessage() . "<br/>";
                            exit;
                        }

                    }
                }
            }
        }
    //}

    $new_last_album_id = 0;
    $new_last_picture_id = 0;



    $query = 'SELECT * FROM albums ORDER BY count_id DESC LIMIT 1;';

    foreach ($conn->query($query) as $row) {
        $new_last_album_id = (int) $row['count_id'];
    }

    $query = 'SELECT * FROM pictures ORDER BY count_id DESC LIMIT 1;';
    foreach ($conn->query($query) as $row) {
        $new_last_picture_id = (int) $row['count_id'];
    }

    //dd(($new_last_album_id != $last_album_id && $new_last_picture_id != $last_picture_id));

    if($new_last_album_id != $last_album_id && $new_last_picture_id != $last_picture_id) {

        $first_album_id = 0;
        $first_picture_id = 0;

        //dd('asaladsdsdsdssdkklklklklaaaa');

        $query = 'SELECT * FROM albums ORDER BY count_id ASC LIMIT 1;';
        foreach ($conn->query($query) as $row) {
            $first_album_id = $row['count_id'];
        }

        //dd('asalada');
        //dd($first_album_id);
        //dd($last_album_id);
        //dd('asaaffaahha');

        $query = 'SELECT * FROM albums WHERE count_id BETWEEN :f AND :l;';
        $stmt = $conn->prepare($query);
        $stmt->execute([':f' => $first_album_id, ':l' => $last_album_id]);
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            $query = 'DELETE FROM albums WHERE count_id = :count_id;';
            $stmt = $conn->prepare($query);
            $stmt->execute([':count_id' => $row['count_id']]);

        }

        //dd($first_album_id);
        //dd($last_album_id);
        //dd('asaaaghjhaa');

        $query = 'SELECT * FROM pictures ORDER BY count_id ASC LIMIT 1;';
        foreach ($conn->query($query) as $row) {
            $first_picture_id = $row['count_id'];
        }

        $query = 'SELECT * FROM pictures WHERE count_id BETWEEN :f AND :l;';
        $stmt = $conn->prepare($query);
        $stmt->execute([':f' => $first_picture_id, ':l' => $last_picture_id]);
        $rows = $stmt->fetchAll();

        foreach ($rows as $row) {
            $query = 'DELETE FROM pictures WHERE count_id = :count_id;';
            $stmt = $conn->prepare($query);
            $stmt->execute([':count_id' => $row['count_id']]);

        }

        unset($stmt);
    }

    //echo "<br> SUCCESS <br>";
} catch (Facebook\Exceptions\FacebookResponseException $e) {
    echo 'Albuns Message: ' . $e->getMessage();
}
