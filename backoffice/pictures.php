<?php
require_once "mysql.php";


$albums = [];

$query = 'SELECT * FROM albums;';
foreach ($conn->query($query) as $album) {

    $query = 'SELECT * FROM excluded_albums WHERE album_id = :album_id;';
    $stmt = $conn->prepare($query);
    $stmt->execute([':album_id' => $album['id']]);
    $excluded = $stmt->fetchAll();
    if(sizeof($excluded)==0) {
        array_push($albums, $album);

        $query = $conn->prepare('SELECT * FROM pictures WHERE albums_id = :id;');
        $query->execute(array(':id' => $album['id']));
        $pictures = $query->fetchAll(\PDO::FETCH_ASSOC);

        $albums[sizeof($albums) - 1]['pictures'] = [];

        foreach ($pictures as $picture) {
            array_push($albums[sizeof($albums) - 1]['pictures'], $picture);
        }

        unset($query);
    }
}
//dd($albums);

echo json_encode(['albums'=>$albums]);
header('Content-Type: application/json; charset=utf-8');