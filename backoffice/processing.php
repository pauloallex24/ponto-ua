<?php
require_once '../vendor/autoload.php';
require_once 'mysql.php';
require_once 'helper.php';
use Intervention\Image\ImageManagerStatic as Image;
#Image::configure(array('driver' => 'imagick'));

if (session_status() === PHP_SESSION_NONE){session_start();}

if(isset($_SESSION['access_token'])) {

    if (isset($_GET['albumtextdelete'])) {
        $query = 'SELECT * FROM excluded_albums WHERE id = :id LIMIT 1;';
        $stmt = $conn->prepare($query);
        $stmt->execute([':id' => $_GET['albumtextdelete']]);
        $excluded = $stmt->fetchAll();
        if (sizeof($excluded) >= 1) {
            $query = 'DELETE FROM excluded_albums WHERE id = :id LIMIT 1;';
            $stmt = $conn->prepare($query);
            $stmt->execute([':id' => $_GET['albumtextdelete']]);
        }
    }

    if (isset($_GET['albumhide'])) {
        $query = 'SELECT * FROM albums WHERE id = :id;';
        $stmt = $conn->prepare($query);
        $stmt->execute([':id' => $_GET['albumhide']]);
        $excluded = $stmt->fetchAll();

        if (sizeof($excluded) >= 1) {
            $query = 'INSERT INTO excluded_albums (album_id) VALUES (:album_id);';
            $stmt = $conn->prepare($query);
            $stmt->execute([':album_id' => $_GET['albumhide']]);
        }
    }

    if (isset($_GET['albumshow'])) {
        $query = 'SELECT * FROM albums WHERE id = :id;';
        $stmt = $conn->prepare($query);
        $stmt->execute([':id' => $_GET['albumshow']]);
        $excluded = $stmt->fetchAll();

        if (sizeof($excluded) >= 1) {
            $query = 'DELETE FROM excluded_albums WHERE album_id = :album_id;';
            $stmt = $conn->prepare($query);
            $stmt->execute([':album_id' => $_GET['albumshow']]);
        }
    }

    if (isset($_GET['cantina'])) {
        if (((int)$_GET['cantina']) > 0 && ((int)$_GET['cantina']) < 4) {
            $query = "UPDATE params SET value = :value WHERE name = 'cantina';";
            $stmt = $conn->prepare($query);
            $stmt->execute([':value' => $_GET['cantina']]);
        }
    }

    if (isset($_GET['showbuilding'])) {
        $query = "UPDATE buildings SET active = 1 WHERE number = :number;";
        $stmt = $conn->prepare($query);
        $stmt->execute([':number' => $_GET['showbuilding']]);
        header('Location: ../backoffice.php#building_' . $_GET['showbuilding']);
        exit;
    }

    if (isset($_GET['hidebuilding'])) {
        $query = "UPDATE buildings SET active = 0 WHERE number = :number;";
        $stmt = $conn->prepare($query);
        $stmt->execute([':number' => $_GET['hidebuilding']]);
        header('Location: ../backoffice.php#building_' . $_GET['hidebuilding']);
        exit;
    }

    if (isset($_GET['deletebuilding'])) {
        $query = "DELETE FROM buildings WHERE number = :number;";
        $stmt = $conn->prepare($query);
        $stmt->execute([':number' => $_GET['deletebuilding']]);

        $query = 'SELECT * FROM buildings ORDER BY number LIMIT 1;';
        foreach ($conn->query($query) as $row) {
            $excluded = $row;
        }

        header('Location: ../backoffice.php#building_' . $excluded['number']);
        exit;
    }

    if (isset($_POST['albumtextadd'])) {
        if (strlen($_POST['albumtextadd']) > 0) {
            $query = 'INSERT INTO excluded_albums (album_name) VALUES (:album_name);';
            $stmt = $conn->prepare($query);
            $stmt->execute([':album_name' => $_POST['albumtextadd']]);
        }
    }

    if (isset($_POST['email'])) {
            $query = "UPDATE params SET value = :value WHERE name = 'email';";
            $stmt = $conn->prepare($query);
            $stmt->execute([':value' => $_POST['email']]);
    }

    if (isset($_POST['emailserver'])) {
            $query = "UPDATE params SET value = :value WHERE name = 'emailserver';";
            $stmt = $conn->prepare($query);
            $stmt->execute([':value' => $_POST['emailserver']]);
    }

    if (isset($_POST['emailport'])) {
            $query = "UPDATE params SET value = :value WHERE name = 'emailport';";
            $stmt = $conn->prepare($query);
            $stmt->execute([':value' => $_POST['emailport']]);
    }

    if (isset($_POST['password']) || isset($_POST['password_confirm'])) {
        if (isset($_POST['password']) && !isset($_POST['password_confirm']) ||
            isset($_POST['password_confirm']) && !isset($_POST['password'])
        ) {
            header('Location: ../backoffice.php?passerror=1#alterar_password');
        } else if ($_POST['password_confirm'] == $_POST['password'] &&
            strlen($_POST['password']) > 5 && strlen($_POST['password']) < 255
        ) {
            $query = "UPDATE params SET value = :value WHERE name = 'password';";
            $stmt = $conn->prepare($query);
            $stmt->execute([':value' => $_POST['password']]);
            header('Location: ../backoffice.php?passok=1#alterar_password');
            exit;
        } else if (strlen($_POST['password']) == 0 && strlen($_POST['password_confirm']) == 0) {
            echo "<!-- -->";
        } else {
            header('Location: ../backoffice.php?passerror=1#alterar_password');
            exit;
        }

    }

    $query = 'SELECT * FROM buildings ORDER BY number;';
    $fields = ['picture', 'number', 'acronym', 'x', 'y', 'name', 'url', 'content'];
    $building_changed = false;
    foreach ($conn->query($query) as $row) {
        foreach ($fields as $field) {
            if (isset($_POST[$field . '_' . $row['number']])) {
                if ($_POST[$field . '_' . $row['number']] != $row[$field]) {
                    $query = "UPDATE buildings SET " . $field . " = :value WHERE number = :number;";
                    $stmt = $conn->prepare($query);
                    $stmt->execute([
                        ':number' => $row['number'],
                        ':value' => $_POST[$field . '_' . $row['number']]
                    ]);
                    $building_changed = true;
                }

            }
        }
        if ($_FILES['picture_' . $row['number']]['size'] > 0)
        {
            try {
                $image = Image::make($_FILES['picture_' . $row['number']]['tmp_name']);
                $image->save('../assets/buildings/' . $row['number'] . '.jpg',100);
            } catch(Exception $e) {
                dd($e);
            }

            $query = "UPDATE buildings SET picture = :value WHERE number = :number;";
            $stmt = $conn->prepare($query);
            $stmt->execute([
                ':number' => $row['number'],
                ':value' =>  $row['number'] . '.jpg'
            ]);
            $building_changed = true;
        }
    }

    if ($building_changed) {
        $query = "SELECT * FROM buildings ORDER BY number ASC LIMIT 1;";
        foreach ($conn->query($query) as $row) {
            $building = $row;
        }
        header('Location: ../backoffice.php#building_' . $building['number']);
        exit;
    }


    $building_set = false;
    foreach ($fields as $field) {
        if(isset($_POST[$field])) {
            if(strlen($_POST[$field])>0) {
                $building_set = true;
            }
        }
    }

    $errors = [];
    if($building_set) {
        if(!((int)$_POST['number'] > 0)) {
            array_push($errors, 'Número de departamento inválido.');
        }
        $query = "SELECT * FROM buildings WHERE number = :number LIMIT 1;";
        $stmt = $conn->prepare($query);
        $stmt->execute([':number'=>$_POST['number']]);
        $build = $stmt->fetchAll();
        if(sizeof($build)>0) {
            array_push($errors, 'O Número do departamento não pode existir.');
        }
        if(!(strlen($_POST['acronym'])>=2 && strlen($_POST['acronym']) <= 10 )) {
            array_push($errors, 'Acrónimo inválido.');
        }
        if(!((int)$_POST['x'] > 0 && (int)$_POST['x'] <= 1620)) {
            array_push($errors, 'Coordenada X inválida.');
        }
        if(!((int)$_POST['y'] > 0 && (int)$_POST['y'] <= 1080)) {
            array_push($errors, 'Coordenada Y inválida.');
        }
        if(!(strlen($_POST['name'])>=6 && strlen($_POST['name']) <= 255 )) {
            array_push($errors, 'Nome inválido.');
        }
        if(!(strlen($_POST['url'])>=6 && strlen($_POST['url']) <= 255 )) {
            array_push($errors, 'URL inválida.');
        }
        if(!(strlen($_POST['content'])>=5 && strlen($_POST['content']) <= 1500 )) {
            array_push($errors, 'Conteúdo inválido.');
        }
        if ($_FILES['picture']['size'] > 0) {
            if(sizeof($errors)==0) {
                try {
                    $image = Image::make($_FILES['picture']['tmp_name']);
                    $image->save('../assets/buildings/' . $_POST['number'] . '.jpg', 100);
                } catch (Exception $e) {
                    dd($e);
                    array_push($errors, 'Imagem inválida.');
                }
            }
        } else {
            array_push($errors, 'Imagem inválida.');
        }


        if(sizeof($errors)==0) {
            $query = 'INSERT INTO buildings (name,number,acronym,content,url,x,y,picture) VALUES (:name,:number,:acronym,:content,:url,:x,:y,:picture);';
            $stmt = $conn->prepare($query);
            try {
                $stmt->execute([
                    ':name' => $_POST['name'],
                    ':number' => $_POST['number'],
                    ':acronym' => $_POST['acronym'],
                    ':content' => $_POST['content'],
                    ':url' => $_POST['url'],
                    ':x' => $_POST['x'],
                    ':y' => $_POST['y'],
                    ':picture' => $_POST['number'] . '.jpg'
                ]);
            } catch (Exception $e) {
                dd($e);
            }

            header('Location: ../backoffice.php?novodepartamento=1#novo_departamento');exit;

        } else {
            $_SESSION['building_errors'] = $errors;
            $_SESSION['name'] = $_POST['name'];
            $_SESSION['number'] = $_POST['number'];
            $_SESSION['acronym'] = $_POST['acronym'];
            $_SESSION['content'] = $_POST['content'];
            $_SESSION['url'] = $_POST['url'];
            $_SESSION['x'] = $_POST['x'];
            $_SESSION['y'] = $_POST['y'];
            header('Location: ../backoffice.php#novo_departamento');exit;
        }
    }


    if(getimagesize($_FILES["upload_mapa"]['tmp_name'])!== false){
        try {
            $image = Image::make($_FILES["upload_mapa"]['tmp_name']);
            $image->save('../assets/buildings/mapa.jpg',100);
        } catch(Exception $e) {
            dd($e);
        }
    }

}

header('Location: ../backoffice.php');