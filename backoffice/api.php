<?php

require_once '../vendor/php-graph-sdk-5.0.0/src/Facebook/autoload.php';
require_once '../config.php';
require_once 'helper.php';
require_once 'mysql.php';
header('Content-Type: application/json; charset=utf-8');
$data = [];

if(isset($_GET['cantina'])) {

    $query = 'SELECT * FROM params WHERE name = :name;';
    $stmt = $conn->prepare($query);
    $stmt->execute([':name' => 'cantina']);
    $default = $stmt->fetchAll();

    $data['default'] = $default[0]['value'];
}


if(isset($_GET['albuns'])) {
    $albums = [];
    $query = 'SELECT * FROM albums;';
    foreach ($conn->query($query) as $album) {

        $query = 'SELECT * FROM excluded_albums WHERE album_id = :album_id;';
        $stmt = $conn->prepare($query);
        $stmt->execute([':album_id' => $album['id']]);
        $excluded = $stmt->fetchAll();
        if(sizeof($excluded)==0) {
            array_push($albums, $album);

            $query = $conn->prepare('SELECT * FROM pictures WHERE albums_id = :id;');
            $query->execute(array(':id' => $album['id']));
            $pictures = $query->fetchAll(\PDO::FETCH_ASSOC);

            $albums[sizeof($albums) - 1]['pictures'] = [];

            foreach ($pictures as $picture) {
                array_push($albums[sizeof($albums) - 1]['pictures'], $picture);
            }

            unset($query);
        }
    }
    $data['albuns'] = $albums;
}

if(isset($_GET['updatealbuns'])) {
    $query = 'SELECT * FROM facebook ORDER BY id DESC LIMIT 1;';
    $stmt = $conn->prepare($query);
    $stmt->execute();
    $token_db = $stmt->fetchAll();
    ini_set('session.gc_maxlifetime', 14400);
    session_set_cookie_params(14400);
    $fb = new Facebook\Facebook([
        'app_id' => $facebook_api_app_id,
        'app_secret' => $facebook_api_app_secret,
        'default_graph_version' => 'v2.8',
    ]);
    if (session_status() === PHP_SESSION_NONE){session_start();}
    $_SESSION['access_token'] = $token_db[0]['accesstoken'];
    if(isset($_SESSION['access_token'])) {
        $expires = time() + 60 * 60 * 2;
        $accessToken = new Facebook\Authentication\AccessToken($_SESSION['access_token'], $expires);
        $fb->setDefaultAccessToken($accessToken->getValue());
        $_SESSION['access_token'] = (string)$accessToken->getValue();
        require_once 'facebook_albuns.php';
        $data['updatealbuns'] = true;
    }
}

if(isset($_GET['mapa'])) {
    $query = 'SELECT * FROM buildings;';
    $stmt = $conn->prepare($query);
    $stmt->execute();
    $data['mapa'] = $stmt->fetchAll();

}


echo json_encode($data);
