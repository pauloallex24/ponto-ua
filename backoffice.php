<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ponto.UA Backoffice</title>
    <script src="config.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/signin.css" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/backoffice.css" crossorigin="anonymous">
    <script src="config.js"></script>
    <script>
        if(!debug) {
            console.log = function() {};
        }
    </script>
</head>
<body>

<?php

require_once __DIR__ . '/vendor/php-graph-sdk-5.0.0/src/Facebook/autoload.php';
require_once 'config.php';
require_once 'backoffice/helper.php';
require_once 'backoffice/mysql.php';


ini_set('session.gc_maxlifetime', 14400);
session_set_cookie_params(14400);
if (session_status() === PHP_SESSION_NONE){session_start();}
$fb = new Facebook\Facebook([
    'app_id' => $facebook_api_app_id,
    'app_secret' => $facebook_api_app_secret,
    'default_graph_version' => 'v2.8',
]);
if(isset($_POST['pass'])) {

    $query = "SELECT * FROM params WHERE name = 'password' LIMIT 1;";
    $password = '';
    foreach ($conn->query($query) as $row) {
        $password = $row['value'];
    }
    if($_POST['pass'] == $password) {
        $_SESSION['session'] = true;
        $_SESSION['new_session'] = true;
    }
    header('Location: backoffice.php?passerrors=true');
    exit;
} else if(!isset($_SESSION['session'])) {
    $_SESSION['new_session'] = false;
    ?>

    <!-- LOGIN -->
    <div class="container">

        <form method="post" class="form-signin" style="">
            <?php if(isset($_GET['passerrors'])) {?>
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Erro:</span>
                    Password Errada
                </div>
            <?php } ?>
            <h2 class="form-signin-heading">Ponto.UA</h2>
            <h3 class="form-signin-heading">Faça Login</h3>
            <label for="inputPassword" class="sr-only">Password</label>
            <br>
            <input type="password" id="pass" name="pass" class="form-control" minlength="6" placeholder="Password" required>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
        </form>

    </div>


    <?php
}
if(isset($_SESSION['new_session']) && $_SESSION['new_session']) {
    $helper = $fb->getRedirectLoginHelper();

    $accessToken = $helper->getAccessToken();
    if (isset($accessToken)) {
        $oAuth2Client = $fb->getOAuth2Client();
        $_SESSION['access_token'] = (string) $accessToken;
        $_SESSION['new_session'] = false;
        unset($_SESSION['new_session']);
    } else {
        $loginUrl = $helper->getLoginUrl($domain . '/backoffice.php');
        ?>

        <!-- LOGIN -->
        <div class="container">

            <form class="form-signin">
                <h2 class="form-signin-heading">Ponto.UA</h2>
                <br>
                <div class="alert alert-info" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Informação:</span>
                    Para avançar, é necessário fazer login com o facebook.
                </div>
                <br>
                <a href="<?php echo $loginUrl; ?>" class="btn btn-lg btn-primary btn-block">Login Facebook</a>
            </form>

        </div>


        <?php
    }
}
if(isset($_SESSION['access_token']) && isset($_SESSION['session']) && !isset($_SESSION['new_session'])) {
    if(isset($_GET['code'])) {
        header('Location: backoffice.php');
        exit;
    }
    $expires = time() + 60 * 60 * 24 * 60;
    $accessToken = new Facebook\Authentication\AccessToken($_SESSION['access_token'], $expires);
    $fb->setDefaultAccessToken($accessToken->getValue());
    $_SESSION['access_token'] = (string) $accessToken->getValue();

    $response = $fb->get('/me');
    $userNode = $response->getGraphUser();

    try {
        $stmt = $conn->prepare(
            'INSERT INTO facebook ( facebook_id, name, accesstoken, created_at) VALUES (:facebook_id, :name, :accesstoken, :created_at)'
        );


        $facebook_id = $userNode->getField('id');
        $name = $userNode->getName();
        $token = $accessToken->getValue();
        $created_at = time();

        $stmt->bindValue(':facebook_id', $facebook_id);
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':accesstoken', $token);
        $stmt->bindValue(':created_at', $created_at);

        if (!$stmt->execute()) {
            echo "PDO Error 1.1:\n";
            print_r($stmt->errorInfo());
            exit;
        }
        unset($stmt);
    } catch (PDOException $e) {
        print "PDO Statement Error!: " . $e->getMessage() . "<br/>";
        exit;
    }

    if (isset($_GET['updatefacebookalbuns'])) {
        require_once 'backoffice/facebook_albuns.php';
    }


    //echo $_SESSION['access_token'] . "<br>";
    //echo '<p>Olá ' . $userNode->getName() . " with the id " . $userNode->getField('id');

    //echo "<br><br>";

    //$albums_edge = $fb->get('/universidadedeaveiro/albums')->getGraphEdge();

    #require 'backoffice/facebook_albuns.php';


    ?>

    <form method="post" enctype="multipart/form-data" action="backoffice/processing.php">

        <div class="navbar navbar-default navbar-fixed-top" style="margin-bottom: 25px;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Navegação</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Ponto.UA</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Álbuns<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#anchor_albuns">Atualizar</a></li>
                            <li><a href="#anchor_excluir_albuns">Excluir por album</a></li>
                            <li><a href="#anchor_excluir_albuns_nome">Excluir por nome</a></li>
                        </ul>
                    </li>
                    <li><a href="#anchor_alterar_cantina">Cantina</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Departamentos<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#anchor_departamentos">Departamentos</a></li>
                            <li><a href="#anchor_criar_departamento">Criar Departamento</a></li>
                            <li><a href="#anchor_mapa">Upload Mapa</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Outras Definições<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#anchor_alterar_password">Password</a></li>
                            <li><a href="#anchor_alterar_email">E-mail</a></li>
                        </ul>
                    </li>
                </ul>

            </div><!--/.nav-collapse -->
        </div>
        <div class="container">
            <br><br>
            <div class="container">
                <div class="row">
                    <a href="backoffice/logout.php" class="btn btn-default" style="margin-right: 5px; display: inline-block;">
                        <span class="glyphicon glyphicon-log-out"></span>
                        <span>Logout</span>
                    </a>
                    <span style="font-size: 16px;">Olá <?php echo $userNode->getName(); ?>!</span>
                </div>
                <br>
            </div>


            <!--
            <div class="container">
                <div class="alert alert-success">
                    <strong>Success!</strong> Indicates a successful or positive action.
                </div>
            </div>
-->
            <div class="container">

                <div class="anchor" id="anchor_albuns"></div>
                <h3 class="" id="albuns">Álbuns</h3>
                <div class="container">
                    <div class="col-md-3">
                        <a href="?updatefacebookalbuns=1" class="btn btn-lg btn-primary btn-block">
                            <span class="glyphicon glyphicon-refresh"></span>
                            Atualizar Albuns
                        </a>
                    </div>
                </div>


                <h4>Excluir Álbuns por listagem:</h4>
                <div class="anchor" id="anchor_excluir_albuns"></div>
                <div class="row excluir_albuns " id="excluir_albuns">
                    <?php

                    $query = 'SELECT * FROM albums;';
                    foreach ($conn->query($query) as $row) {

                        $query = 'SELECT * FROM excluded_albums WHERE album_id = :album_id ORDER BY id DESC LIMIT 1;';
                        $stmt = $conn->prepare($query);
                        $stmt->execute([':album_id' => $row['id']]);
                        $excluded = $stmt->fetchAll();
                        ?>

                        <div class="col-md-4">
                            <div class="input-group">
                                <a href="backoffice/processing.php?album<?php echo ((sizeof($excluded)>=1)?'show':'hide'); ?>=<?php echo $row['id']; ?>"
                                   class="ocultar input-group-addon btn-<?php echo ((sizeof($excluded)>=1)?'warning':'success'); ?> glyphicon glyphicon-<?php echo ((sizeof($excluded)>=1)?'minus':'ok'); ?>">
                                    <span class="titulo"><?php echo ((sizeof($excluded)>=1)?'Ocultado':'A Mostrar'); ?></span>
                                </a>
                                <span class="form-control"><?php echo $row['name']; ?></span>
                            </div><!-- /input-group -->
                        </div><!-- /.col-md-4 -->


                    <?php } ?>
                </div>
            </div>
            <br><br>
            <h4>Ocultar Álbuns por nome:</h4>

            <div class="container">
                <div class="anchor" id="anchor_excluir_albuns_nome"></div>
                <div class="row excluir_albuns" id="excluir_albuns_nome">

                    <?php

                    $query = "SELECT * FROM excluded_albums WHERE album_id IS NULL AND album_name <> '';";
                    foreach ($conn->query($query) as $row) {
                        ?>

                        <div class="col-md-6">
                            <div class="input-group">
                                <a href="backoffice/processing.php?albumtextdelete=<?php echo $row['id']; ?>"
                                   class="delete input-group-addon btn btn-danger glyphicon glyphicon-remove">
                                    <span class="titulo">Apagar</span>
                                </a>
                                <span class="form-control"><?php echo $row['album_name']; ?></span>
                            </div><!-- /input-group -->
                        </div><!-- /.col-md-6 -->

                    <?php } ?>

                </div>
                <br>
                <label>Adicionar novo:</label>
                <div class="anchor" id="anchor_excluir_albuns_nome_novo"></div>
                <div class="row " id="excluir_albuns_nome_novo">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon btn btn-primary"
                                  onclick="$('#excluir').val('')">
                                <span class="glyphicon glyphicon-remove"></span>
                                <span class="titulo">Limpar</span>
                            </span>
                            <input id="albumtextadd" name="albumtextadd" class="form-control" type="text" placeholder="Nome a excluir " />
                        </div><!-- /input-group -->
                    </div><!-- /.col-md-6 -->
                </div>
            </div>


        </div>
        <br><br><br><br>
        <div class="anchor" id="anchor_alterar_cantina"></div>
        <div class="container " id="cantina_predefinida">
            <h3>Cantina Pré-definida:</h3>
            <br>
            <?php

            $query = "SELECT * FROM params WHERE name = 'cantina' LIMIT 1;";
            $row = '';
            foreach ($conn->query($query) as $cantina) {
                $row = $cantina;
            }
            ?>
            <div class="row " id="alterar_cantina">
                <div class="col-md-12 col-centered input-group">
                    <a href="backoffice/processing.php?cantina=1" class="ocultar btn btn-<?php echo (((int)$row['value']==1)?'primary':'default');?>" style="margin-right: 15px;">
                        <span class="glyphicon glyphicon-home"></span>
                        <span>Santiago</span>
                        <input type="radio" name="cantina[]" value="santiago" <?php echo (((int)$row['value']==1)?'checked':'');?>>
                    </a>
                    <a href="backoffice/processing.php?cantina=2" class="ocultar btn btn-<?php echo (((int)$row['value']==2)?'primary':'default');?>" style="margin-right: 15px;">
                        <span class="glyphicon glyphicon-home"></span>
                        <span>Crasto</span>
                        <input type="radio" name="cantina[]" value="santiago" <?php echo (((int)$row['value']==2)?'checked':'');?>>
                    </a>
                    <a href="backoffice/processing.php?cantina=3" class="ocultar btn btn-<?php echo (((int)$row['value']==3)?'primary':'default');?>" style="margin-right: 15px;">
                        <span class="glyphicon glyphicon-home"></span>
                        <span>Snack-Bar</span>
                        <input type="radio" name="cantina[]" value="santiago" <?php echo (((int)$row['value']==3)?'checked':'');?>>
                    </a>
                </div>
            </div>
        </div>





        <br><br><br><br>
        <div class="container">
            <h3>Departamentos:</h3>
            <div class="anchor" id="anchor_departamentos"></div>

            <div id="departamentos" class="">
                <div class="container">

                    <div class="panel-group">

                        <?php

                        $query = 'SELECT * FROM buildings ORDER BY number;';
                        foreach ($conn->query($query) as $row) {

                            ?>
                            <div class="panel panel-primary">
                                <a class="anchor" id="building_<?php echo $row['number']; ?>"></a>
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse<?php echo $row['number']; ?>"><?php echo $row['number'] . ' - ' . $row['acronym']; ?></a>
                                    </h4>

                                    <a href="backoffice/processing.php?deletebuilding=<?php echo $row['number']; ?>" class="btn btn-danger pull-right delete">
                                        <span class="glyphicon glyphicon-remove"></span>
                                        <span class="titulo">Apagar</span>
                                    </a>


                                    <a href="backoffice/processing.php?<?php
                                    echo (($row['active']=='0')?'show':'hide'); ?>building=<?php echo $row['number']; ?>"
                                       class="btn btn-<?php echo (($row['active']=='0')?'warning':'success'); ?> pull-right ocultar" style="margin-right: 25px;">

                                        <span class="glyphicon glyphicon-<?php echo (($row['active']=='0')?'minus':'ok'); ?>-sign"></span>
                                        <span class="titulo"><?php echo (($row['active']=='0')?'Ocultado':'A Mostrar'); ?></span>
                                    </a>
                                    <!--
                                    <div class="btn btn-info pull-right" style="margin-right: 5px;">
                                        <span class="glyphicon glyphicon-ok-sign"></span>
                                        <span class="titulo">Mostrar</span>
                                        <input type="radio" id="mostrar_1" name="mostrar_1" value="mostrar" checked>
                                    </div>

                                    -->

                                </div>
                                <div id="collapse<?php echo $row['number']; ?>" class="panel-collapse collapse form-group">
                                    <div class="container">
                                        <div class="pagination-centered" style="padding-left: 0 !important; text-align: center;">
                                            <div class="row" style="text-align: center;">
                                                <div class="col-md-3">
                                                    <span id="show_<?php echo $row['number']; ?>" class="cover btn btn-default" onclick="$('#picture_<?php echo $row['number']; ?>').trigger('click');"
                                                          style="background-image: url(<?php echo $domain . '/assets/buildings/' . $row['picture']; ?>)">

                                                    </span>
                                                    <input type="file" accept="image/*" code="<?php echo $row['number']; ?>" class="form-control-file inserted dep"
                                                           id="picture_<?php echo $row['number']; ?>" name="picture_<?php echo $row['number']; ?>">
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="row">
                                                        <div class="col-md-2 input-group">
                                                            <span class="input-group-addon" id="basic-addon1">Nº</span>
                                                            <input class="form-control" min="1" id="number_<?php echo $row['number']; ?>" value="<?php echo $row['number']; ?>"
                                                                   name="number_<?php echo $row['number']; ?>" type="number" placeholder="Num.">
                                                        </div>

                                                        <div class="col-md-3 input-group">
                                                            <span class="input-group-addon" id="basic-addon1">Acrón</span>
                                                            <input class="form-control" id="acronym_<?php echo $row['number']; ?>" value="<?php echo $row['acronym']; ?>"
                                                                   name="acronym_<?php echo $row['number']; ?>" type="text" placeholder="Acrónimo" minlength="2" maxlength="255" >
                                                        </div>

                                                        <div class="col-md-2 input-group">
                                                            <span class="input-group-addon" id="basic-addon1">X</span>
                                                            <input class="form-control" id="x_<?php echo $row['number']; ?>" value="<?php echo $row['x']; ?>"
                                                                   name="x_<?php echo $row['number']; ?>" type="number" placeholder="Posição X" min="1" max="1620">
                                                        </div>
                                                        <div class="col-md-2 input-group">
                                                            <span class="input-group-addon" id="basic-addon1">Y</span>
                                                            <input class="form-control" id="y_<?php echo $row['number']; ?>" value="<?php echo $row['y']; ?>"
                                                                   name="y_<?php echo $row['number']; ?>" type="number" placeholder="Posição Y" min="1" max="1080">
                                                        </div>

                                                        <div class="col-md-1 input-group">
                                                            <button class="btn btn-default" onclick="window.open('backoffice/mapa.php?x=' + $('#x_<?php echo $row['number']; ?>').val() + '&y=' + $('#y_<?php echo $row['number']; ?>').val() +  '&name=' + $('#acronym_<?php echo $row['number']; ?>').val());return false;">
                                                                <span class="titulo">Ver no mapa</span>
                                                            </button>
                                                        </div>

                                                        <div class="col-md-12 input-group">
                                                            <span class="input-group-addon" id="basic-addon1">Nome</span>
                                                            <input class="form-control" id="name_<?php echo $row['number']; ?>" value="<?php echo $row['name']; ?>"
                                                                   name="name_<?php echo $row['number']; ?>" type="text" placeholder="Nome" minlength="6" maxlength="255" >
                                                        </div>

                                                        <div class="col-md-12 input-group">
                                                            <span class="input-group-addon" id="basic-addon1">Acrón</span>
                                                            <input class="form-control" id="url_<?php echo $row['number']; ?>" value="<?php echo $row['url']; ?>"
                                                                   name="url_<?php echo $row['number']; ?>" type="text" placeholder="URL" minlength="6" maxlength="255" >
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                            <textarea class="form-control"  minlength="5" maxlength="1500" id="content_<?php echo $row['number']; ?>"
                                                      name="content_<?php echo $row['number']; ?>"><?php echo $row['content']; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>



                        <br><br>

                        <div class="anchor" id="anchor_criar_departamento"></div>
                        <label class="" id="criar_departamento">Novo Departamento:</label>

                        <?php

                        if(isset($_SESSION['building_errors'])) {
                            ?>

                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Erro:</span>
                                <b>Foram detetados os seguintes erros:</b>
                                <ul>
                                    <?php
                                    foreach ($_SESSION['building_errors'] as $error) {
                                        echo '<li>' . $error . '</li>';
                                    } ?>
                                </ul>
                            </div>

                            <?php
                            unset($_SESSION['building_errors']);
                        }

                        if(isset($_GET['novodepartamento'])) {
                            ?>

                            <div class="alert alert-success" role="alert">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span class="sr-only">Sucesso:</span>
                                Departamento criado!
                            </div>

                            <?php
                        }

                        ?>

                        <div class="anchor" id="anchor_novo_departamento"></div>

                        <div id="novo_departamento" class="form-group " style="padding: 15px 0; background-color: #ffffff;">
                            <div class="container">
                                <div class="pagination-centered" style="padding-left: 0 !important; text-align: center;">
                                    <div class="row" style="text-align: center;">
                                        <div class="col-md-3">
                                            <span id="show" class="cover btn btn-default" onclick="$('#picture').trigger('click');"
                                                  style="">
                                                <span class="glyphicon glyphicon-picture"></span>
                                                <span style="display: block;">Fotografia Departamento</span>
                                            </span>
                                            <input type="file" accept="image/*" class="form-control-file inserted dep" name="picture" id="picture">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-2 input-group">
                                                        <span class="input-group-addon" id="basic-addon1">Nº</span>
                                                        <input class="form-control" min="1" id="number" value="<?php echo (isset($_SESSION['number'])?$_SESSION['number']:''); ?>"
                                                               name="number" type="number" placeholder="Num.">
                                                    </div>

                                                    <div class="col-md-3 input-group">
                                                        <span class="input-group-addon" id="basic-addon1">Acrón</span>
                                                        <input class="form-control" id="acronym" value="<?php echo (isset($_SESSION['acronym'])?$_SESSION['acronym']:''); ?>"
                                                               minlength="2" maxlength="255" name="acronym" type="text" placeholder="Acrónimo">
                                                    </div>

                                                    <div class="col-md-2 input-group">
                                                        <span class="input-group-addon" id="basic-addon1">X</span>
                                                        <input class="form-control" id="x" value="<?php echo (isset($_SESSION['x'])?$_SESSION['x']:''); ?>"
                                                               name="x" min="1" max="1620" type="number" placeholder="Posição X">
                                                    </div>
                                                    <div class="col-md-2 input-group">
                                                        <span class="input-group-addon" id="basic-addon1">Y</span>
                                                        <input class="form-control" id="y" value="<?php echo (isset($_SESSION['y'])?$_SESSION['y']:''); ?>"
                                                               name="y" min="1" max="1080" type="number" placeholder="Posição Y">
                                                    </div>
                                                    <div class="col-md-1 input-group">
                                                        <button class="btn btn-default" onclick="window.open(domain + '/backoffice/mapa.php?x=' + $('#x').val() + '&y=' + $('#y').val() +  '&name=' + $('#acronym').val());return false;">
                                                            <span class="titulo">Ver no mapa</span>
                                                        </button>
                                                    </div>

                                                    <div class="col-md-12 input-group">
                                                        <span class="input-group-addon" id="basic-addon1">Nome</span>
                                                        <input class="form-control" id="name" value="<?php echo (isset($_SESSION['name'])?$_SESSION['name']:''); ?>"
                                                               minlength="6" maxlength="255" name="name" type="text" placeholder="Nome">
                                                    </div>

                                                    <div class="col-md-12 input-group">
                                                        <span class="input-group-addon" id="basic-addon1">URL</span>
                                                        <input class="form-control" id="url" value="<?php echo (isset($_SESSION['url'])?$_SESSION['url']:''); ?>"
                                                               minlength="6" maxlength="255" name="url" type="text" placeholder="URL">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea class="form-control" id="content" name="content" minlength="5" maxlength="255" ><?php echo (isset($_SESSION['content'])?$_SESSION['content']:''); ?></textarea>
                                    </div>

                                    <?php

                                    if(isset($_SESSION['name'])) {unset($_SESSION['name']);}
                                    if(isset($_SESSION['number'])) {unset($_SESSION['number']);}
                                    if(isset($_SESSION['acronym'])) {unset($_SESSION['acronym']);}
                                    if(isset($_SESSION['content'])) {unset($_SESSION['content']);}
                                    if(isset($_SESSION['url'])) {unset($_SESSION['url']);}
                                    if(isset($_SESSION['x'])) {unset($_SESSION['x']);}
                                    if(isset($_SESSION['y'])) {unset($_SESSION['y']);}

                                    ?>
                                </div>
                            </div>
                        </div>


                        <br>
                        <div class="anchor" id="anchor_mapa"></div>
                        <label class="" id="mapa">Upload de novo mapa (background):</label>
                        <div class="form-group">
                            <input type="file" accept="image/*" class="form-control-file" id="upload_mapa" name="upload_mapa" aria-describedby="fileHelp">

                            <a href="assets/buildings/mapa.jpg" class="btn btn-default" target="_blank">
                                <span class="titulo">Ver Mapa</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <br><br><hr><br>
        <div class="container">
            <h3>Alterar Password:</h3>

            <div class="anchor" id="anchor_alterar_password"></div>
            <div class="container " id="alterar_password">

                <?php if(isset($_GET['passerror'])){ ?>
                    <div class="alert alert-danger" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Erro:</span>
                        Passwords diferentes
                    </div>
                <?php } ?>

                <?php if(isset($_GET['passok'])){ ?>
                    <div class="alert alert-success" role="alert">
                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                        <span class="sr-only">Ótimo:</span>
                        Password alterada
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                                <span class="input-group-addon btn btn-primary"
                                      onclick="$('#password').val('');">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    <span class="titulo">Limpar</span>
                                </span>
                            <input id="password" name="password" class="form-control" type="text" placeholder="Password" />
                        </div><!-- /input-group -->
                    </div><!-- /.col-md-6 -->
                    <div class="col-md-6">
                        <div class="input-group">
                                <span class="input-group-addon btn btn-primary"
                                      onclick="$('#password_confirm').val('');">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    <span class="titulo">Limpar</span>
                                </span>
                            <input id="password_confirm" name="password_confirm" class="form-control" type="text" placeholder="Confirmar Password" />
                        </div><!-- /input-group -->
                    </div><!-- /.col-md-6 -->
                </div>


            </div>
        </div>

        <?php
        $query = "SELECT * FROM params WHERE name = 'email' LIMIT 1;";
        $row = '';
        foreach ($conn->query($query) as $email) {
            $row = $email;
        }
        ?>
        <div class="anchor" id="anchor_alterar_email"></div>

        <div class="container " id="alterar_email">
            <h3>Alterar e-mail de notificação:</h3>
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group">
                                <span class="input-group-addon btn btn-primary"
                                      onclick="$('#email').val('');">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    <span class="titulo">Limpar</span>
                                </span>
                        <input id="email" name="email" class="form-control"
                               value="<?php echo $row['value']; ?>" type="text" placeholder="E-mail" />
                    </div><!-- /input-group -->
                </div><!-- /.col-md-6 -->
            </div>
        </div>


        <br><br><br><br><br><br>

        <nav class="navbar navbar-default navbar-fixed-bottom" style="padding-top: 7px;">
            <button type="submit" class="btn btn-primary pull-right" style="margin-right: 15px;">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                <span>Guardar</span>
            </button>

            <button type="button" class="btn btn-default pull-right" style="margin-right: 15px;" onclick="location.reload();">
                <span class="glyphicon glyphicon-repeat"></span>
                <span class="titulo">Limpar Alterações</span>
            </button>

            <button type="button" id="reload_pannel" class="btn btn-danger pull-right" style="margin-right: 15px;">
                <span class="glyphicon glyphicon-export"></span>
                <span class="titulo">Forçar Atualização do Painel</span>
            </button>
            &nbsp;
        </nav>
    </form>
<?php } ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!--<script src="http://socket.pontoua.pneves.pt/socket/socket.io/socket.io.js"></script>-->
<script src="assets/script/socket.io.js"></script>


<script>
    //$('head').append('<scrip' + 't src="' + socket_domain + '/socket/socket.io/socket.io.js"></scri' + 'pt>');
    var socket = io.connect(socket_domain,{ port: socket_server_port});
    socket.on('connect_failed',function(data){
        console.log(data || 'connect_failed');
    });
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="assets/script/backoffice.js"></script>

</body>
</html>
